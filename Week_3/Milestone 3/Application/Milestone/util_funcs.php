<?php

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-05-23
 * Class     : CST-126 Database Application Programming I
 * Professor : Kondo Litchmore PhD.
 * Assignment: Milestone
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. util_funcs.php - a collection of functions
 * 2.
 * 3.
 * 4.
 * ---------------------------------------------------------------
 */

//
// Create a database PDO connection
// Returns the connection
// Throw exception to database_error display form
//
function dbConnect() {

    // Define local / development database connection parameters
    $connect_string = 'mysql:host=localhost:3306;dbname=cst-236';
    $db_username = "root";
    $db_password = "root";

    // Define azure / publish database connection parameters
    // $connect_string = 'mysql:host=127.0.0.1:49921;dbname=cst-126';
    // $db_username = "azure";
    // $db_password = "6#vWHD_$";

    try
    {
        // Create a PDO object
        $db_connection = new PDO($connect_string, $db_username, $db_password);
        $db_connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    } catch (PDOException $e)
    {
        $error_message = $e->getMessage();
        include('database_error.php');
        exit();
    }

    return $db_connection;
}

function getAllUsers()
{
    // Get Database Connection
    $db = dbConnect();

    // Define SQL prepare statement and bind values
    $sql = "SELECT u.* " .
           "  FROM users u " .
           " ORDER BY u.LAST_NAME, u.FIRST_NAME";
    $statement1 = $db->prepare($sql);

    // Execute select query
    $statement1->execute();

    // return records as associative array - could use fetchAll
    $statement1->setFetchMode(PDO::FETCH_ASSOC);

    // add records to users array
    $users = array();
    $index = 0;
    while ($row = $statement1->fetch())
    {
        $users[$index] = array($row["ID"], $row["FIRST_NAME"], $row["LAST_NAME"], $row["EMAIL"], $row["MOBILE"], $row["PASSWORD"], $row["BIRTHDATE"], $row["GENDER"], $row["ROLE_ID"]);
        ++$index;
    }

    // Close statement and connection
    $statement1->closeCursor();
    $statement1 = null;
    $db = null;

    return $users;
}

function saveUserId($id)
{
    session_start();
    $_SESSION["USER_ID"] = $id;
}

function getUserId()
{
    session_start();
    return $_SESSION["USER_ID"];
}

function saveUserFirstName($firstName)
{
    session_start();
    $_SESSION["USER_FIRSTNAME"] = $firstName;
}

function getUserFirstName()
{
    session_start();
    return $_SESSION["USER_FIRSTNAME"];
}

function saveUserLasttName($lastName)
{
    session_start();
    $_SESSION["USER_LASTNAME"] = $lastName;
}

function getUserLastName()
{
    session_start();
    return $_SESSION["USER_LASTNAME"];
}

function saveUserInfo($user_info_array)
{
    session_start();
    $_SESSION["USER_INFO"] = $user_info_array;
}

function getUserInfo()
{
    session_start();
    return $_SESSION["USER_INFO"];
}

function saveSearchInfo($search_info_array)
{
    session_start();
    $_SESSION["SEARCH_INFO"] = $search_info_array;
}

function getSearchInfo()
{
    session_start();
    return $_SESSION["SEARCH_INFO"];
}

?>