<?php
namespace classes\model;
include_once $_SERVER['DOCUMENT_ROOT'] . '/Milestone/AutoLoader.php';

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-10-31
 * Class     : CST-236 Database Application Programming II
 * Professor : Nathan Braun
 * Assignment: Milestone
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Milestone
 * 2. Business Layer
 * 3. Product mode
 * ---------------------------------------------------------------
 */

class Product
{
    private $id;
    private $scancode;
    private $name;
    private $description;
    private $price;
    private $deleted_flag;
    
    public function __construct($id, $scancode, $name, $description, $price, $deleted_flag)
    {
        $this->id = $id;
        $this->scancode = $scancode;
        $this->name = $name;
        $this->description = $description;
        $this->price = $price;
        $this->deleted_flag = $deleted_flag;
    }
    
    /**
     * @return int - product id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string - product scancode
     */
    public function getScanCode()
    {
        return $this->scancode;
    }

    /**
     * @return string - product name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string - product description
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return float - product price
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @param string $scancode
     */
    public function setScanCode($scancode)
    {
        $this->scancode = $scancode;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @param float $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }
    
    /**
     * @return boolean
     */
    public function getDeleted_flag()
    {
        return $this->deleted_flag;
    }
    
    /**
     * @param boolean $deleted_flag
     */
    public function setDeleted_flag($deleted_flag)
    {
        $this->deleted_flag = $deleted_flag;
    }
    
    
    
    
}

