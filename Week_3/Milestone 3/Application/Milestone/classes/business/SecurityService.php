<?php
namespace classes\business;
use classes;

include_once $_SERVER['DOCUMENT_ROOT'] . '/Milestone/AutoLoader.php';

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-10-31
 * Class     : CST-236 Database Application Programming II
 * Professor : Nathan Braun
 * Assignment: Milestone
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Security Service
 * 2. Validate User Login
 * 3. Handle Login Validation
 * ---------------------------------------------------------------
 */

class SecurityService
{
    public function authenticate($email, $password)
    {
        $service = new classes\database\UserDataService();
        return $service->validateUserLogin($email, $password);
    }   
}

