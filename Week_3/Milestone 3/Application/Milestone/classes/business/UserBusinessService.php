<?php
namespace classes\business;
use classes;

include_once $_SERVER['DOCUMENT_ROOT'] . '/Milestone/AutoLoader.php';

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-10-22
 * Class     : CST-236 Database Application Programming II
 * Professor : Nathan Braun
 * Assignment: Milestone
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Milestone
 * 2. Business Layer
 * 3. User information
 * ---------------------------------------------------------------
 */

class UserBusinessService
{

    public function __construct()
    {}
    
    public function searchByFirstName($pattern)
    {
        $service = new classes\database\UserDataService();
        return $service->findByFirstName($pattern);
    }
    
    public function getAllUsers()
    {
        $service = new classes\database\UserDataService();
        return $service->getAllUsers();
    }
    
    public function getUserById($id)
    {
        $service = new classes\database\UserDataService();
        return $service->getUserById($id);
    }
    
    public function deleteUserById($id)
    {
        $service = new classes\database\UserDataService();
        return $service->delete($id);
    }
    
    public function updateUser($user)
    {
        $service = new classes\database\UserDataService();
        return $service->update($user);
    }
    
    public function createUser($user)
    {
        $service = new classes\database\UserDataService();
        return $service->create($user);
    }
    
}

