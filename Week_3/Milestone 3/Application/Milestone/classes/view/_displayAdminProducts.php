<?php
namespace classes\view;
include_once $_SERVER['DOCUMENT_ROOT'] . '/Milestone/AutoLoader.php';
?>

<!--
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-10-31
 * Class     : CST-236 Database Application Programming II
 * Professor : Nathan Braun
 * Assignment: Milestone
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Milestone
 * 2. Display table of products for edit
 * 2. Reusable functions
 * ---------------------------------------------------------------
 -->

<table id="post_entries">
  <thead>
    <tr>
        <th>ID</th>
        <th>Scan Code</th>
        <th>Name</th>
        <th>Description</th>
        <th>Price</th>
        <th>Action</th>
    </tr>
  </thead>
  <tbody>

<?php
    foreach ($products as $p)
    {
        echo "  <tr>\n";
        echo "      <td>" . $p[0] . "</td>\n";
        echo "      <td>" . $p[1] . "</td>\n";
        echo "      <td>" . $p[2] . "</td>\n";
        echo "      <td>" . $p[3] . "</td>\n";
        echo "      <td>" . $p[4] . "</td>\n";
        echo "      <td>" . "<a href='../controller/ProductChangeHandler.php?id=" . $p[0] . "&mode=0'>Edit<a>" .
             "&nbsp;|&nbsp;<a href='../controller/ProductChangeHandler.php?id=" . $p[0] . "&mode=1'>Delete<a></td>\n";

        echo "  </tr>\n";
	}
 ?>
</tbody>
</table>
