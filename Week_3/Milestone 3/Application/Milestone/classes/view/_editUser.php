<?php
namespace classes\view;
include_once $_SERVER['DOCUMENT_ROOT'] . '/Milestone/AutoLoader.php';
session_start();
?>

<!DOCTYPE html>

<!--
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-10-31
 * Class     : CST-236 Database Application Programming II
 * Professor : Nathan Braun
 * Assignment: Milestone
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Edit Users
 * 2. Simple place holder
 * 3. TO DO: Add card/image/detail
 * ---------------------------------------------------------------
 -->

<html>
<head>
<meta charset="ISO-8859-1">
<link rel=stylesheet href="../../css/main_nav.css" />
<title>STORE NAME HERE</title>
</head>
<body>

<?php require_once('../../util_funcs.php');?>
<?php require_once '../../_main_menu.php';?>


	<div align="center">
    	<hr><br />
    	<h1>Welcome - STORE NAME HERE!</h1>
    	<hr><br />
 	<div align="center">
		<form action="../controller/UserEditHandler.php" method="POST">
		    <h1>Edit User Form</h1>
		    <p>Please fill in this form to update the account.</p>
		    <hr><br />

			<input type="hidden"  name="UserID" id="UserID" <?php echo 'value="' . $user_row[0]['ID'] . '"'; ?> >

		    <label for="FirstName"><b>First Name:</b></label>
		    <input type="text" placeholder="Enter first name" name="FirstName" id="FirstName" required <?php echo 'value="' . $user_row[0]['FIRST_NAME'] . '"'; ?> ><br /><br />

		    <label for="LastName"><b>Last Name:</b></label>
		    <input type="text" placeholder="Enter last name" name="LastName" id="LastName" required <?php echo 'value="' . $user_row[0]['LAST_NAME'] . '"'; ?> ><br /><br />

		    <label for="Email"><b>Email:</b></label>
		    <input type="email" placeholder="Enter email address" name="Email" id="Email" required <?php echo 'value="' . $user_row[0]['EMAIL'] . '"'; ?> ><br /><br />

		    <label for="Mobile"><b>Mobile:</b></label>
		    <input type="tel" placeholder="Enter mobile number" name="Mobile" id="Mobile" required <?php echo 'value="' . $user_row[0]['MOBILE'] . '"'; ?> ><br /><br />

		    <label for="Password"><b>Password:</b></label>
		    <input type="text" placeholder="Password Length 8 minimum" name="Password" id="Password" pattern=".{8,}" required <?php echo 'value="' . $user_row[0]['PASSWORD'] . '"'; ?> ><br /><br />

		    <label for="Birthdate"><b>Birth Date:</b></label>
		    <input type="date" placeholder="Enter birthdate" name="Birthdate" id="Birthdate" required <?php echo 'value="' . $user_row[0]['BIRTHDATE'] . '"'; ?> ><br /><br />

<?php

    echo "<label for=\"Gender\"><b>Sex:</b></label>\n";
    echo "<select name=\"Gender\" id=\"Gender\" required>\n";
    echo "<option value=\"0\" ";
        if ($user_row[0]['GENDER'] == 0) echo "selected";
    echo ">Male</option>\n";
    echo "<option value=\"1\" ";
        if ($user_row[0]['GENDER'] == 1) echo "selected";
    echo ">Female</option>\n";
    echo "</select><br /><br />\n";

    echo "<label for=\"UserRoleID\">Select Role:</label>\n";
    echo "<select name=\"UserRoleID\" id=\"UserRoleID\">\n";
    for($x=0; $x < count($roles); $x++)
    {
        echo "<option value=\"" . $roles[$x]['ID'] . "\"";
        if ($roles[$x]['ID'] == $user_row[0]['ROLE_ID'])
            echo " selected>";
        else
            echo ">";
            echo $roles[$x]['ROLENAME'] . "</option>\n";
    }
    echo "</select><br /><br />\n";
?>


		    <button type="submit">Update</button><br /><br />
		    <hr>
		</form>
	</div>
	</div>

</body>
</html>
