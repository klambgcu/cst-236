<?php
namespace classes\view;
include_once $_SERVER['DOCUMENT_ROOT'] . '/Milestone/AutoLoader.php';
session_start();
?>

<!DOCTYPE html>

<!--
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-10-31
 * Class     : CST-236 Database Application Programming II
 * Professor : Nathan Braun
 * Assignment: Milestone
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Create Products
 * 2. Simple place holder
 * 3. TO DO: Add card/image/detail
 * ---------------------------------------------------------------
 -->

<html>
<head>
<meta charset="ISO-8859-1">
<link rel=stylesheet href="../../css/main_nav.css" />
<title>STORE NAME HERE</title>
</head>
<body>

<?php require_once('../../util_funcs.php');?>
<?php require_once '../../_main_menu.php';?>


	<div align="center">
    	<hr><br />
    	<h1>Welcome - STORE NAME HERE!</h1>
    	<hr><br />
 	<div align="center">
		<form action="../controller/ProductCreateHandler.php" method="POST">
		    <h1>Create Product Form</h1>
		    <p>Please fill in this form to create a product.</p>
		    <hr><br />

		    <label for="ScanCode"><b>Scan Code:</b></label>
		    <input type="text" placeholder="Enter scan code" name="ScanCode" id="ScanCode" required ><br /><br />

		    <label for="Name"><b>Name:</b></label>
		    <input type="text" placeholder="Enter product name" name="Name" id="Name" required ><br /><br />

		    <label for="Description"><b>Description:</b></label>
		    <input type="text" placeholder="Enter description" name="Description" id="Description" required ><br /><br />

		    <label for="Price"><b>Price:</b></label>
		    <input type="text" placeholder="Enter price" name="Price" id="Price" required ><br /><br />

		    <button type="submit">Create</button><br /><br />
		    <hr>
		</form>
	</div>
	</div>

</body>
</html>
