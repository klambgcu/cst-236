<?php
namespace classes\controller;
use classes;
include_once $_SERVER['DOCUMENT_ROOT'] . '/Milestone/AutoLoader.php';

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-10-31
 * Class     : CST-236 Database Application Programming II
 * Professor : Nathan Braun
 * Assignment: Milestone
 * Disclaimer: This is my own work
* ---------------------------------------------------------------
* Description:
* 1. Create Product Handler (ProductCreateHandler.php)
* 2. Retrieves fields from createProduct.php
* 3. Stores in database
* ---------------------------------------------------------------
*/

require_once('../../util_funcs.php');

// store registration parameters
$product_id  = filter_input(INPUT_POST,'ProductID');
$scancode    = filter_input(INPUT_POST,'ScanCode');
$name        = filter_input(INPUT_POST,'Name');
$description = filter_input(INPUT_POST,'Description');
$price       = filter_input(INPUT_POST,'Price');
$deleted_flag = 0; // Not deleted

// Instantiate object and service and send product to update
$product = new classes\model\Product($product_id, $scancode, $name, $description, $price, $deleted_flag);

$service = new classes\business\ProductBusinessService();
$result = $service->updateProduct($product);

if (! $result)
{
    print_r($product);
    $error_message = "Update Product Failed - Contact Administrator";
    include('../database/database_error.php');
    exit();
}


header('Location: ../view/admin_edit_product.php');

?>
