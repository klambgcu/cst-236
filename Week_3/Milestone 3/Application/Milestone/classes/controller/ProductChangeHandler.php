<?php
namespace classes\controller;
use classes;
include_once $_SERVER['DOCUMENT_ROOT'] . '/Milestone/AutoLoader.php';

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-10-31
 * Class     : CST-236 Database Application Programming II
 * Professor : Nathan Braun
 * Assignment: Milestone
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Milestone - Handle Post Edit / Delete Requests
 * 2. Obtain form data
 * 3. Handle Login Validation
 * ---------------------------------------------------------------
 */

require_once('../../util_funcs.php');

// store registration parameters
$product_id = filter_input(INPUT_GET,'id');
$mode       = filter_input(INPUT_GET,'mode'); // 0 - Edit, 1 - Delete (Disable for now)

// Validate mode operations
if ( ($mode < 0) || ($mode > 1) )
{
    echo "Invalid Request Operation - Contact Administrator.<br />";
    exit();
}

try
{
    // Get Database Connection
    $db = dbConnect();

    // Define SQL prepare statement and bind values
    $sql = "SELECT * " .
           "  FROM products " .
           " WHERE ID = :product_id";
    $statement1 = $db->prepare($sql);
    $statement1->bindValue(':product_id', $product_id);

    // Execute query
    $statement1->execute();
    $product_row = $statement1->fetchAll(\PDO::FETCH_ASSOC);

} catch(\PDOException $e)
{
    $error_message = $e->getMessage();
    include('../database/database_error.php');
    exit();
}

// Close statement and connection
$statement1->closeCursor();
$statement1 = null;
$db = null;

if ($mode == 0)
{
    include('../view/_editProduct.php');
}
else
{
    include('../view/_deleteProduct.php');
}
?>