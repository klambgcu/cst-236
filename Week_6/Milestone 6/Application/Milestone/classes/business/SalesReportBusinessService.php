<?php
namespace classes\business;
use classes;

require_once $_SERVER['DOCUMENT_ROOT'] . '/Milestone/AutoLoader.php';

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-11-18
 * Class     : CST-236 Database Application Programming II
 * Professor : Nathan Braun
 * Assignment: Milestone
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Milestone
 * 2. Business Layer
 * 3. Sales Report Information
 * ---------------------------------------------------------------
 */

class SalesReportBusinessService
{

    public function __construct()
    {}
    
    // Return an array containing results of report
    // Params - date range start, end
    public function getProductSalesReportByDateRange($start_date, $end_date)
    {
        $service = new classes\database\SalesReportDataService();
        return $service->getProductSalesReportByDateRange($start_date, $end_date);
    }
}

