-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Nov 21, 2021 at 07:25 AM
-- Server version: 5.7.24
-- PHP Version: 7.4.1

SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cst-236`
--
CREATE DATABASE IF NOT EXISTS `cst-236` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `cst-236`;

-- --------------------------------------------------------

--
-- Table structure for table `addresses`
--

DROP TABLE IF EXISTS `addresses`;
CREATE TABLE IF NOT EXISTS `addresses` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `USER_ID` int(11) NOT NULL,
  `ADDRESS_TYPE` int(11) NOT NULL,
  `STREET` varchar(100) NOT NULL,
  `CITY` varchar(100) NOT NULL,
  `STATE` varchar(100) NOT NULL,
  `POSTAL` varchar(100) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `USER_ID_IDX` (`USER_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `addresses`
--

TRUNCATE TABLE `addresses`;
--
-- Dumping data for table `addresses`
--

INSERT INTO `addresses` (`ID`, `USER_ID`, `ADDRESS_TYPE`, `STREET`, `CITY`, `STATE`, `POSTAL`) VALUES
(1, 4, 1, '54321 Another Street Dr.', 'Another City', 'AZ', '85542'),
(2, 4, 2, '12345 Some Street Rd.', 'Some City', 'CA', '95954'),
(3, 4, 1, '12345 Some Street Rd.', 'Some City', 'AZ', '85643'),
(4, 4, 2, '121231 street', 'city', 'CA', '90686'),
(5, 7, 1, '8888 Skywalker Ranch Rd.', 'Las Vegas', 'NV', '72654'),
(6, 7, 2, '8888 Skywalker Ranch Rd', 'Las Vegas', 'NV', '72654'),
(7, 8, 1, '1 Tree Hill Rd.', 'Sherwood Forest', 'AK', '10123'),
(8, 8, 2, '1 Tree Hill Rd.', 'Sherwood Forest', 'AK', '10123'),
(9, 10, 1, '100 Castle Dr.', 'Fantasy', 'CA', '96542'),
(10, 10, 2, '100 Castle Dr.', 'Fantasy', 'CA', '96542');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
CREATE TABLE IF NOT EXISTS `orders` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `DATE` date NOT NULL,
  `USER_ID` int(11) NOT NULL,
  `BILLING_ADDRESS_ID` int(11) NOT NULL,
  `SHIPPING_ADDRESS_ID` int(11) NOT NULL,
  `TOTAL_COST` float NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `ADDRESSES_ID_IDX1` (`BILLING_ADDRESS_ID`),
  KEY `ADDRESSES_ID_IDX2` (`SHIPPING_ADDRESS_ID`),
  KEY `USER_ID_IDX1` (`USER_ID`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `orders`
--

TRUNCATE TABLE `orders`;
--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`ID`, `DATE`, `USER_ID`, `BILLING_ADDRESS_ID`, `SHIPPING_ADDRESS_ID`, `TOTAL_COST`) VALUES
(1, '2021-11-15', 4, 1, 2, 612.65),
(2, '2021-11-15', 4, 3, 4, 13.96),
(3, '2021-11-21', 7, 5, 6, 2043.53),
(4, '2021-11-21', 8, 7, 8, 1040.88),
(5, '2021-11-21', 10, 9, 10, 5399.76);

-- --------------------------------------------------------

--
-- Table structure for table `order_details`
--

DROP TABLE IF EXISTS `order_details`;
CREATE TABLE IF NOT EXISTS `order_details` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ORDER_ID` int(11) NOT NULL,
  `PRODUCT_ID` int(11) NOT NULL,
  `PRICE` float NOT NULL,
  `QTY` int(11) NOT NULL,
  `EXTENDED_COST` float NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `ORDERS_ID_IDX2` (`ORDER_ID`) USING BTREE,
  KEY `PRODCTS_ID_IDX2` (`PRODUCT_ID`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `order_details`
--

TRUNCATE TABLE `order_details`;
--
-- Dumping data for table `order_details`
--

INSERT INTO `order_details` (`ID`, `ORDER_ID`, `PRODUCT_ID`, `PRICE`, `QTY`, `EXTENDED_COST`) VALUES
(1, 1, 7, 13.96, 10, 139.6),
(2, 1, 32, 94.61, 5, 473.05),
(3, 2, 7, 13.96, 1, 13.96),
(4, 3, 1, 103, 1, 103),
(5, 3, 7, 13.96, 1, 13.96),
(6, 3, 9, 83.02, 9, 747.18),
(7, 3, 46, 27.87, 1, 27.87),
(8, 3, 51, 10, 24, 240),
(9, 3, 54, 18.99, 48, 911.52),
(10, 4, 55, 123.49, 6, 740.94),
(11, 4, 56, 49.99, 6, 299.94),
(12, 5, 57, 249.99, 12, 2999.88),
(13, 5, 58, 199.99, 12, 2399.88);

-- --------------------------------------------------------

--
-- Table structure for table `order_history`
--

DROP TABLE IF EXISTS `order_history`;
CREATE TABLE IF NOT EXISTS `order_history` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `EVENTDATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `EVENTTYPE` int(11) NOT NULL,
  `ORDER_ID` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `ORDERS_ID_IDX4` (`ORDER_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `order_history`
--

TRUNCATE TABLE `order_history`;
--
-- Dumping data for table `order_history`
--

INSERT INTO `order_history` (`ID`, `EVENTDATE`, `EVENTTYPE`, `ORDER_ID`) VALUES
(1, '2021-11-15 04:49:14', 0, 1),
(2, '2021-11-15 05:35:06', 0, 2),
(3, '2021-11-21 05:23:17', 0, 3),
(4, '2021-11-21 05:45:45', 0, 4),
(5, '2021-11-21 06:14:17', 0, 5);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
CREATE TABLE IF NOT EXISTS `products` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `SCANCODE` varchar(100) NOT NULL,
  `NAME` varchar(100) NOT NULL,
  `DESCRIPTION` varchar(100) NOT NULL,
  `PRICE` float NOT NULL,
  `IMAGE` varchar(100) NOT NULL DEFAULT 'product_default.jpg',
  `DELETED_FLAG` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `SCANCODE_UNIQUE` (`SCANCODE`),
  UNIQUE KEY `NAME_UNIQUE` (`NAME`)
) ENGINE=InnoDB AUTO_INCREMENT=59 DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `products`
--

TRUNCATE TABLE `products`;
--
-- Dumping data for table `products`
--

INSERT INTO `products` (`ID`, `SCANCODE`, `NAME`, `DESCRIPTION`, `PRICE`, `IMAGE`, `DELETED_FLAG`) VALUES
(1, 'PRD000-1', 'Listerine FreshBurst Antiseptic', 'Unsp open wound of r rng fngr w/o damage to nail, sequela', 103, 'product_default.jpg', 0),
(2, 'PRD000-2', 'RhinoBlaster', 'Sprain of other specified parts of thorax, initial encounter', 56.1, 'product_default.jpg', 0),
(3, 'PRD000-3', 'Allergenic Extracts Standardized Mite', 'Toxic effect of tobacco cigarettes, accidental, sequela', 45.57, 'product_default.jpg', 0),
(4, 'PRD000-4', 'LYCOPODIUM CLAVATUM', 'Combined systolic and diastolic (congestive) hrt fail', 28.93, 'product_default.jpg', 0),
(5, 'PRD000-5', 'simvastatin', 'Amphetamines', 46.56, 'product_default.jpg', 0),
(6, 'PRD000-6', 'Long Last Makeup 03', 'Displacement of intrauterine contraceptive device, sequela', 98.74, 'product_default.jpg', 0),
(7, 'PRD000-7', 'ACME FIRST AID Contains 404 PIECES', 'Malig neoplasm of prph nerves and autonm nervous sys, unsp', 13.96, 'product_default.jpg', 0),
(8, 'PRD000-8', 'Mefenamic Acid', 'Problems related to release from prison', 36.58, 'product_default.jpg', 0),
(9, 'PRD000-9', 'Crest Pro-Health', 'Path fracture in oth disease, unsp tibia and fibula, sequela', 83.02, 'product_default.jpg', 0),
(10, 'PRD000-10', 'Sei Bella Age-Defying Liquid Foundation', 'Car occupant injured in collision w statnry object', 89.85, 'product_default.jpg', 0),
(11, 'PRD000-11', 'Trandolapril', 'Smallpox vaccines', 73.17, 'product_default.jpg', 0),
(12, 'PRD000-12', 'Metronidazole', 'Congenital shortening of left upper limb', 86.69, 'product_default.jpg', 0),
(13, 'PRD000-13', 'Nephrocaps', 'Idiopathic aseptic necrosis of unspecified ankle', 87.12, 'product_default.jpg', 0),
(14, 'PRD000-14', 'ZONISAMIDE', 'Straining to void', 70.2, 'product_default.jpg', 0),
(15, 'PRD000-15', 'Losortan Potassium', 'Displaced fracture of navicular of unspecified foot, sequela', 76.7, 'product_default.jpg', 0),
(16, 'PRD000-16', 'ESIKA Extreme Moisturizing SPF 16', 'Unspecified sprain of left lesser toe(s)', 25.72, 'product_default.jpg', 0),
(17, 'PRD000-17', 'Yeastrol', 'Monoplg upr lmb fol oth cerebvasc disease aff right dom side', 65.12, 'product_default.jpg', 0),
(18, 'PRD000-18', 'Clonidine Hydrochloride', 'Unsp fx shaft of right ulna, init for opn fx type I/2', 94.39, 'product_default.jpg', 0),
(19, 'PRD000-19', 'Clobetasol Propionate', 'Obstructed labor due to oth malpos and malpresent, fetus 4', 32.34, 'product_default.jpg', 0),
(20, 'PRD000-20', 'Pure Foam', 'Optic papillitis, unspecified eye', 80.38, 'product_default.jpg', 0),
(21, 'PRD000-21', 'Goose Feathers', 'Occupant of hv veh injured in collision w 2/3-whl mv', 16.39, 'product_default.jpg', 0),
(22, 'PRD000-22', 'Nifedipine', 'Lacerat extn/abdr musc/fasc/tend of thmb at forarm lv, subs', 81.8, 'product_default.jpg', 0),
(23, 'PRD000-23', 'Sodium Phosphates', 'Unsp inj blood vesls at abd, low back and pelvis level, init', 22.42, 'product_default.jpg', 0),
(24, 'PRD000-24', 'Ertaczo', 'Poisn by antihyperlip and antiarterio drugs, assault, init', 24.53, 'product_default.jpg', 0),
(25, 'PRD000-25', 'Glipizide XL', 'Torus fracture of upper end of left tibia', 31.73, 'product_default.jpg', 0),
(26, 'PRD000-26', 'LEVAQUIN', 'Displ spiral fx shaft of ulna, unsp arm, 7thR', 95.55, 'product_default.jpg', 0),
(27, 'PRD000-27', 'Etodolac', 'Effects of other deprivation', 24.25, 'product_default.jpg', 0),
(28, 'PRD000-28', 'RAZADYNE', 'Acute lymphangitis of trunk, unspecified', 72.19, 'product_default.jpg', 0),
(29, 'PRD000-29', 'Antiperspirant Sandalwood', 'Diseases of the circ sys comp pregnancy, third trimester', 22.7, 'product_default.jpg', 0),
(30, 'PRD000-30', 'Sephora Super Lisseur Rides SPF 15 Age Defy Moisture', 'Maternal care for oth fetal problems, first trimester, unsp', 58.26, 'product_default.jpg', 0),
(31, 'PRD000-31', 'Bio Pine', 'Juvenile rheumatoid arthritis with systemic onset, shoulder', 42.77, 'product_default.jpg', 0),
(32, 'PRD000-32', 'Buckwheat', 'Sezary disease, unspecified site', 94.61, 'product_default.jpg', 0),
(33, 'PRD000-33', 'LBEL FILLING EFFECT FOUNDATION SPF 10', 'Unsp inj blood vessels at wrs/hnd lv of right arm, init', 20.07, 'product_default.jpg', 0),
(34, 'PRD000-34', 'SCROPHULOUS', 'Other contact with rat', 26.48, 'product_default.jpg', 0),
(35, 'PRD000-35', 'Smoke Control', 'Aneurysmal bone cyst, right hand', 28.15, 'product_default.jpg', 0),
(36, 'PRD000-36', 'OLUX', 'Nondisp bicondylar fx r tibia, 7thF', 97.11, 'product_default.jpg', 0),
(37, 'PRD000-37', 'Gabapentin', 'Contact with turtles', 45.97, 'product_default.jpg', 0),
(38, 'PRD000-38', 'Kadian', 'Garden or yard of unsp non-institut residence as place', 22.45, 'product_default.jpg', 0),
(39, 'PRD000-39', 'Psoriasin', 'Person outsd hv veh inj in clsn w 2/3-whl mv nontraf, sqla', 43.82, 'product_default.jpg', 0),
(40, 'PRD000-40', 'NovoSeven', 'Oth fx shaft of left ulna, subs for clos fx w delay heal', 11.37, 'product_default.jpg', 0),
(41, 'PRD000-41', 'Old Spice Red Zone Sweat Defense', 'Microcystoid degeneration of retina', 61.78, 'product_default.jpg', 0),
(42, 'PRD000-42', 'SPEEDGEL RX', 'Pnctr w/o fb of unsp external genital organs, male, sequela', 27.79, 'product_default.jpg', 0),
(43, 'PRD000-43', 'Amlodipine Besylate', 'Encounter for issue of medical certificate', 86.44, 'product_default.jpg', 0),
(44, 'PRD000-44', 'KLAR and DANVER ANTIBACTERIAL HAND', 'Unsp subluxation of right sternoclavicular joint, init', 93.16, 'product_default.jpg', 0),
(45, 'PRD000-45', 'Fluorouracil', 'Inflammatory disorders of scrotum', 71.79, 'product_default.jpg', 0),
(46, 'PRD000-46', 'Naproxen', 'Burn of unspecified degree of right lower leg, subs encntr', 27.87, 'product_default.jpg', 0),
(47, 'PRD000-47', 'Olanzapine and Fluoxetine', 'Corrosion of 3rd deg mu sites of unsp wrs/hnd, sequela', 59.5, 'product_default.jpg', 0),
(48, 'PRD000-48', 'Lithium Carbonate', 'Viral warts', 28.55, 'product_default.jpg', 0),
(49, 'PRD000-49', 'CYPROHEPTADINE HYDROCHLORIDE', 'Lacerat unsp blood vessel at wrs/hnd lv of right arm, init', 53.79, 'product_default.jpg', 0),
(50, 'PRD000-50', 'Laxative', 'Mycosis fungoides', 54.06, 'product_default.jpg', 0),
(51, 'PRD000-1-4', 'Amazing BBQ Sauce 32 oz.', 'Best Barbeque Sauce Ever! 32 ounce jar', 10, 'product_default.jpg', 0),
(54, 'PRD000-1-5', 'Amazing BBQ Sauce 64 oz.', 'Best Barbeque Sauce Ever! 64 ounce jar', 18.99, 'product_default.jpg', 0),
(55, 'PRD000-1-1', 'Long Bow (Easy Pull Action)', '48 Inch Long, Dual Action, Scope, Easy Pull Action', 123.49, 'product_default.jpg', 0),
(56, 'PRD000-1-2', 'Quiver Full of Arrows (24)', 'Quiver containing 24 sharp tip arrows (perfect for scaring crooked politicians)', 49.99, 'product_default.jpg', 0),
(57, 'PRD000-1-3', 'Ball Gown White Lace Size 4', 'Fancy Ball Gown White Lace, Gloves & Sash Included', 249.99, 'product_default.jpg', 0),
(58, 'PRD000-1-6', 'Glass Slippers', 'Perfect Slippers For That Special Evening', 199.99, 'product_default.jpg', 0);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
CREATE TABLE IF NOT EXISTS `roles` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ROLENAME` varchar(45) NOT NULL,
  `DESCRIPTION` varchar(45) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `roles`
--

TRUNCATE TABLE `roles`;
--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`ID`, `ROLENAME`, `DESCRIPTION`) VALUES
(1, 'Normal User', 'A Customer'),
(2, 'User Maintenance', 'User Maintenance'),
(3, 'Product Maintenance', 'Product Maintenance'),
(4, 'Admin', 'Administrator Users and Products');

-- --------------------------------------------------------

--
-- Table structure for table `shopping_cart`
--

DROP TABLE IF EXISTS `shopping_cart`;
CREATE TABLE IF NOT EXISTS `shopping_cart` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `USER_ID` int(11) NOT NULL,
  `PRODUCT_ID` int(11) NOT NULL,
  `QTY` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `USERS_ID_IDX3` (`USER_ID`),
  KEY `PRODCTS_ID_IDX3` (`PRODUCT_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `shopping_cart`
--

TRUNCATE TABLE `shopping_cart`;
--
-- Dumping data for table `shopping_cart`
--

INSERT INTO `shopping_cart` (`ID`, `USER_ID`, `PRODUCT_ID`, `QTY`) VALUES
(2, 3, 7, 95),
(3, 3, 3, 1),
(4, 3, 43, 1),
(5, 3, 29, 1),
(6, 3, 31, 1),
(7, 3, 32, 1),
(8, 3, 19, 1),
(9, 3, 18, 1),
(10, 3, 9, 1),
(11, 3, 49, 1),
(12, 3, 50, 1),
(15, 3, 51, 24),
(16, 3, 54, 48),
(23, 7, 51, 12),
(24, 7, 54, 6),
(25, 7, 31, 24);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `FIRST_NAME` varchar(100) NOT NULL,
  `LAST_NAME` varchar(100) NOT NULL,
  `EMAIL` varchar(100) NOT NULL,
  `MOBILE` varchar(100) NOT NULL,
  `PASSWORD` varchar(100) NOT NULL,
  `BIRTHDATE` date NOT NULL,
  `GENDER` tinyint(1) NOT NULL,
  `ROLE_ID` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `EMAIL` (`EMAIL`),
  KEY `ROLE_ID_idx` (`ROLE_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `users`
--

TRUNCATE TABLE `users`;
--
-- Dumping data for table `users`
--

INSERT INTO `users` (`ID`, `FIRST_NAME`, `LAST_NAME`, `EMAIL`, `MOBILE`, `PASSWORD`, `BIRTHDATE`, `GENDER`, `ROLE_ID`) VALUES
(1, 'User', 'Maint', 'umaint@store.com', '(111) 111-1111', '11111111', '2000-01-01', 0, 2),
(2, 'Prod', 'Maint', 'pmaint@store.com', '(222) 222-2222', '22222222', '2000-01-01', 0, 3),
(3, 'Ad', 'min', 'admin@store.com', '(333) 333-3333', '33333333', '2000-01-01', 0, 4),
(4, 'Kelly', 'Lamb', 'kl@kl.com', '(562) 555-1234', '12345678', '1965-05-01', 0, 1),
(5, 'Deena', 'Lamb', 'dl@dl.com', '(888) 888-8888', '12345678', '2000-07-20', 1, 1),
(7, 'Anakin', 'Skywalker', 'askywalker@sky.net', '(777) 737-7257', '11111111', '2000-01-01', 0, 1),
(8, 'Robin', 'Hood', 'rhood@nottingham.org', '(222) 222-3333', '11111111', '1999-12-25', 0, 1),
(9, 'Anthony', 'Stark', 'tony@starkhq.com', '(414) 143-1918', '77777777', '1960-05-01', 0, 1),
(10, 'Sleeping', 'Beauty', 'sb@beauty.org', '(123) 123-4567', '22222222', '1974-05-13', 1, 1);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `addresses`
--
ALTER TABLE `addresses`
  ADD CONSTRAINT `USERS_ID_FX` FOREIGN KEY (`USER_ID`) REFERENCES `users` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `USERS_ID_FX1` FOREIGN KEY (`USER_ID`) REFERENCES `users` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `order_details`
--
ALTER TABLE `order_details`
  ADD CONSTRAINT `ORDERS_ID_FK` FOREIGN KEY (`ORDER_ID`) REFERENCES `orders` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `PRODUCTS_ID_FX1` FOREIGN KEY (`PRODUCT_ID`) REFERENCES `products` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `order_history`
--
ALTER TABLE `order_history`
  ADD CONSTRAINT `ORDERS_ID_FK4` FOREIGN KEY (`ORDER_ID`) REFERENCES `orders` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `shopping_cart`
--
ALTER TABLE `shopping_cart`
  ADD CONSTRAINT `PRODUCTS_ID_FX4` FOREIGN KEY (`PRODUCT_ID`) REFERENCES `products` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `USERS_ID_FX4` FOREIGN KEY (`USER_ID`) REFERENCES `users` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `ROLE_ID` FOREIGN KEY (`ROLE_ID`) REFERENCES `roles` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;
SET FOREIGN_KEY_CHECKS=1;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
