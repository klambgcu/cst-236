<?php

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-11-15
 * Class     : CST-236 Database Application Programming II
 * Professor : Nathan Braun
 * Assignment: Activity 6
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Activity 6
 * 2. Configuration
 * 3. Automatically load dependencies
 * ---------------------------------------------------------------
 */

spl_autoload_register(function($className)
{    
    $className = str_replace("\\", DIRECTORY_SEPARATOR, $className);
    // include_once __DIR__ . DIRECTORY_SEPARATOR . $className . '.php';
    include_once $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'Topic6' . DIRECTORY_SEPARATOR . $className . '.php';
    
});
    
?>
