<?php
require_once 'AutoLoader.php';

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-11-15
 * Class     : CST-236 Database Application Programming II
 * Professor : Nathan Braun
 * Assignment: Activity 6
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Activity 6
 * 2. Configuration
 * 3. User REST API (JSON)
 * ---------------------------------------------------------------
 */

$users = array();

for ($i = 0; $i < 10; $i++)
{
    $users[$i] = new User($i, "First " . $i, "Last " . $i);
}

header('Content-Type: application/json; charset=utf-8');
echo json_encode($users);

?>
