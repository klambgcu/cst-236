<?php

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-10-22
 * Class     : CST-236 Database Application Programming II
 * Professor : Nathan Braun
 * Assignment: Milestone
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Milestone
 * 2. Configuration
 * 3. Automatically load dependencies
 * ---------------------------------------------------------------
 */

spl_autoload_register(function($className) {
    
        // echo "INSIDE AutoLoader - Step 1<br />\r\n";
        // echo __DIR__ . DIRECTORY_SEPARATOR . $className . "<br />\r\n";
    
    $className = str_replace("\\", DIRECTORY_SEPARATOR, $className);
    // include_once __DIR__ . DIRECTORY_SEPARATOR . $className . '.php';
    include_once $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'Milestone' . DIRECTORY_SEPARATOR . $className . '.php';
    
});
    
?>
