<?php
namespace classes\business;
use classes;

require_once $_SERVER['DOCUMENT_ROOT'] . '/Milestone/AutoLoader.php';

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-10-22
 * Class     : CST-236 Database Application Programming II
 * Professor : Nathan Braun
 * Assignment: Milestone
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Milestone
 * 2. Business Layer
 * 3. Role information
 * ---------------------------------------------------------------
 */

class RoleBusinessService
{

    public function __construct()
    {}
        
    public function getAllRoles()
    {
        $service = new classes\database\RoleDataService();
        return $service->getAllRoles();
    }
    
    public function getRoleById($id)
    {
        $service = new classes\database\RoleDataService();
        return $service->getRoleById($id);
    }
    
    
}

?>
