<?php
namespace classes\business;
use classes;

require_once $_SERVER['DOCUMENT_ROOT'] . '/Milestone/AutoLoader.php';

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-11-14
 * Class     : CST-236 Database Application Programming II
 * Professor : Nathan Braun
 * Assignment: Milestone
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Milestone
 * 2. Business Layer
 * 3. Order Information
 * ---------------------------------------------------------------
 */

class OrderBusinessService
{

    public function __construct()
    {}
    
    public function validateCreditCard($credit_card)
    {
        return true; // This would simulate validating/charging credit card
    }
    
    // Check Out Process - Create database entries for all required entities
    public function createOrder($user_id, $shipAddress, $billAddress, $cart_list, $cart_cost, $credit_card)
    {
        $card_ok = $this->validateCreditCard($credit_card);
        if ($card_ok) // Good to process order.
        {
            $service = new classes\database\OrderDataService();
            return $service->create($user_id, $shipAddress, $billAddress, $cart_list, $cart_cost);
        }
        else
        {
            return 0; // credit card not acceptable
        }
    }

}

