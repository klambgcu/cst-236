<?php
require_once 'Autoloader.php';

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-11-10
 * Class     : CST-236 Database Application Programming II
 * Professor : Nathan Braun
 * Assignment: Activity 5
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Activity 5
 * 2. Business Layer
 * 3. Banking Transaction Account Information (ACID Database)
 * ---------------------------------------------------------------
 */

class BankBusinessService
{
    public function __construct()
    {}
    
    public function getCheckingBalance()
    {
        // Get a connection to the database using PDO
        $database = new Database();
        $conn = $database->getConnect();

        // Get and return the checking balance
        $service = new CheckingAccountDataService($conn);
        
        // Close connection and database
        $conn = null;
        $database = null;
        
        return $service->getBalance();
    }
    
    public function getSavingBalance()
    {
        // Get a connection to the database using PDO
        $database = new Database();
        $conn = $database->getConnect();
        
        // Get and return the saving balance
        $service = new SavingAccountDataService($conn);

        // Close connection and database
        $conn = null;
        $database = null;
        
        return $service->getBalance();
    }
        
    public function transaction()
    {           
        // Get a connection to the database
        $database = new Database();
        $conn = $database->getConnect();
        
        // Turn auto commit off - not correct for PDO 
        // $this->conn->autocommit(FALSE);
        
        // Begin a transaction - PDO auto commit off here.
        $conn->beginTransaction();

        // Subtract 100 from checking
        $checking_service = new CheckingAccountDataService($conn);
        $balance = $checking_service->getBalance() - 100;
        $checking_ok = $checking_service->updateBalance($balance);
        
        // add 100 to saving
        $saving_service = new SavingAccountDataService($conn);       
        $balance = $saving_service->getBalance() + 100;
        $saving_ok = $saving_service->updateBalance($balance);
        
        if ($checking_ok && $saving_ok)
        {
            echo "<br>committed<br><br>\n";
            $conn->commit();
        }
        else 
        {
            echo "<br>rolled back<br><br>\n";
            $conn->rollBack();
        }

        // Close connection and database
        $conn = null;
        $database = null;
        
    }
}

