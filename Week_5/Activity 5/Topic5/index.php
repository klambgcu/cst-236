<?php
require_once 'Autoloader.php';

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-11-10
 * Class     : CST-236 Database Application Programming II
 * Professor : Nathan Braun
 * Assignment: Activity 5
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Activity 5
 * 2. View Layer
 * 3. Testing Data Service Functionality
 * ---------------------------------------------------------------
 */

$bs = new BankBusinessService();
$checkbalance = $bs->getCheckingBalance();
$savingbalance = $bs->getSavingBalance();

echo "Current values:<br>\n";
echo "Checking balance = " . $checkbalance . "<br>\n";
echo "Saving balance = " . $savingbalance . "<br>\n";

echo "<br><hr>\n";

$bs->transaction();

$checkbalance = $bs->getCheckingBalance();
$savingbalance = $bs->getSavingBalance();

echo "Current values:<br>\n";
echo "Checking balance = " . $checkbalance . "<br>\n";
echo "Saving balance = " . $savingbalance . "<br>\n";





?>
