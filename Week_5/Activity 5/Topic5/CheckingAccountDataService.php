<?php
require_once 'Autoloader.php';

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-11-10
 * Class     : CST-236 Database Application Programming II
 * Professor : Nathan Braun
 * Assignment: Activity 5
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Activity 5
 * 2. Persistence Layer
 * 3. Checking Account Information
 * ---------------------------------------------------------------
 */

class CheckingAccountDataService
{
    private $conn;
    
    public function __construct($conn)
    {
        $this->conn = $conn;
    }
    
    public function getBalance()
    {        
        $id = 1; // hard code account for now :)
        
        // Define SQL prepare statement and bind values
        $sql = "SELECT BALANCE " .
               "  FROM CHECKING " .
               " WHERE ID = :id";
        $statement = $this->conn->prepare($sql);
        
        $statement->bindValue(':id', $id);
        
        // Execute select query
        $statement->execute();
        
        // return records as associative array - could use fetchAll
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        
        $row = $statement->fetch();
        
        $balance = $row["BALANCE"];
        
        return $balance;
    }
    
    public function updateBalance($balance)
    {
        try
        {
            $id = 1; // hard code account for now :)
            
            // Define SQL prepare statement and bind values
            $sql = " UPDATE CHECKING " .
                   "    SET BALANCE = :balance " .
                   "  WHERE ID = :id";
            $statement = $this->conn->prepare($sql);
            
            $statement->bindValue(':balance', $balance);
            $statement->bindValue(':id',      $id);
            
            // Execute update query
            $statement->execute();
            
        } catch(PDOException $e)
        {
            return 0;
        }
        return 1;
    }
}

