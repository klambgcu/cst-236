<?php

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-11-10
 * Class     : CST-236 Database Application Programming II
 * Professor : Nathan Braun
 * Assignment: Activity 5
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Activity 5
 * 2. Configuration
 * 3. Automatically load dependencies
 * ---------------------------------------------------------------
 */

spl_autoload_register(function($className) {
    
        // echo "INSIDE AutoLoader - Step 1<br />\r\n";
        // echo __DIR__ . DIRECTORY_SEPARATOR . $className . "<br />\r\n";
    
    $className = str_replace("\\", DIRECTORY_SEPARATOR, $className);
    // include_once __DIR__ . DIRECTORY_SEPARATOR . $className . '.php';
    include_once $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'Topic5' . DIRECTORY_SEPARATOR . $className . '.php';
    
});
    
?>
