<?php
namespace classes\controller;
use classes;

require_once $_SERVER['DOCUMENT_ROOT'] . '/Milestone/AutoLoader.php';

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-10-24
 * Class     : CST-236 Database Application Programming II
 * Professor : Nathan Braun
 * Assignment: Milestone
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Milestone - Handle Product Search
 * 2. Obtain form data
 * 3. Handle Data / Display
 * ---------------------------------------------------------------
 */

include_once '../../header.php';
include_once '../../securePage.php';
require_once '../../util_funcs.php';

// Get criteria from product catalog search
$pattern = filter_input(INPUT_POST, "SearchPattern");

// Create a new busienss service object
$service = new classes\business\ProductBusinessService();

// Load data from database for display
$products = $service->searchByProductName($pattern);

saveSearchInfo($products);


// print_r($products);

// Redirect to display data
header('Location: ../view/productCatalog.php');

?>