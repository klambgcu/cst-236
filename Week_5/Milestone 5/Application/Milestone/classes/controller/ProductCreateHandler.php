<?php
namespace classes\controller;
use classes;
require_once $_SERVER['DOCUMENT_ROOT'] . '/Milestone/AutoLoader.php';

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-10-31
 * Class     : CST-236 Database Application Programming II
 * Professor : Nathan Braun
 * Assignment: Milestone
 * Disclaimer: This is my own work
* ---------------------------------------------------------------
* Description:
* 1. Edit Product Handler (ProductEditHandler.php)
* 2. Retrieves fields from _editProduct.php
* 3. Stores in database
* ---------------------------------------------------------------
*/

require_once('../../util_funcs.php');

// store registration parameters
$product_id  = 0;
$scancode    = filter_input(INPUT_POST,'ScanCode');
$name        = filter_input(INPUT_POST,'Name');
$description = filter_input(INPUT_POST,'Description');
$price       = filter_input(INPUT_POST,'Price');
$image       = "product_default.jpg"; // For now 
$deleted_flag = 0; // Not deleted

// Instantiate object and service and send product to update
$product = new classes\model\Product($product_id, $scancode, $name, $description, $price, $image, $deleted_flag);

$service = new classes\business\ProductBusinessService();
$result = $service->createProduct($product);

if (! $result)
{
    print_r($product);
    $error_message = "Create Product Failed - Contact Administrator";
    include('../database/database_error.php');
    exit();
}


header('Location: ../view/admin_edit_product.php');

?>
