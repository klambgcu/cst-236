<?php
namespace classes\view;
use classes;
require_once $_SERVER['DOCUMENT_ROOT'] . '/Milestone/AutoLoader.php';
session_start();
require_once '../../util_funcs.php';
?>

<!DOCTYPE html>

<!--
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-10-31
 * Class     : CST-236 Database Application Programming II
 * Professor : Nathan Braun
 * Assignment: Milestone
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Admin Product Maintenance Listing
 * 2.
 * 3.
 * ---------------------------------------------------------------
 -->

<html>
<head>
<meta charset="ISO-8859-1">
<link rel=stylesheet href="../../css/main_nav.css" />
<link rel=stylesheet href="../../css/post_entries.css" />
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.3/css/jquery.dataTables.css">
<script
  src="https://code.jquery.com/jquery-3.6.0.slim.min.js"
  integrity="sha256-u7e5khyithlIdTpu22PHhENmPcRdFiHRjhAuHcs05RI="
  crossorigin="anonymous">
  </script>

<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.js"></script>
<title>STORE NAME HERE</title>
</head>
<body>

<?php require_once '../../_main_menu.php';?>

	<div align="center">
    	<hr><br />
    	<h1>Welcome - STORE NAME HERE!</h1>
    	<hr><br />
    	<h1>Edit Product Listing</h1><br />
    	<h3><a href="../view/createProduct.php">Create New Product</a></h3>
	</div>

<br />

<?php
    $service = new classes\business\ProductBusinessService();
    $products = $service->getAllProducts();
    include('../view/_displayAdminProducts.php');
?>

<script>
$(document).ready( function () {
	$('#post_entries').DataTable();
} );
</script>

</body>
</html>