<?php
namespace classes\view;
require_once $_SERVER['DOCUMENT_ROOT'] . '/Milestone/AutoLoader.php';
session_start();
?>

<!DOCTYPE html>

<!--
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-10-31
 * Class     : CST-236 Database Application Programming II
 * Professor : Nathan Braun
 * Assignment: Milestone
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Edit Products
 * 2. Simple place holder
 * 3. TO DO: Add card/image/detail
 * ---------------------------------------------------------------
 -->

<html>
<head>
<meta charset="ISO-8859-1">
<link rel=stylesheet href="../../css/main_nav.css" />
<title>STORE NAME HERE</title>
</head>
<body>

<?php require_once('../../util_funcs.php');?>
<?php require_once '../../_main_menu.php';?>
<?php $pModel = $product; ?>

	<div align="center">
    	<hr><br />
    	<h1>Welcome - STORE NAME HERE!</h1>
    	<hr><br />
 	<div align="center">
		<form action="../controller/ProductEditHandler.php" method="POST">
		    <h1>Edit Product Form</h1>
		    <p>Please fill in this form to update the product.</p>
		    <hr><br />

			<img name="ProductImage" id="ProductImage" src="../../images/<?php echo $pModel->getImage() ?> " <?php echo 'value="' . $pModel->getImage() . '"'; ?> ><br /><br />
			<input type="hidden"  name="Image" id="Image" <?php echo 'value="' . $pModel->getImage() . '"'; ?> >
			<input type="hidden"  name="ProductID" id="ProductID" <?php echo 'value="' . $pModel->getId() . '"'; ?> >

		    <label for="ScanCode"><b>Scan Code:</b></label>
		    <input type="text" placeholder="Enter scan code" name="ScanCode" id="ScanCode" required <?php echo 'value="' . $pModel->getScancode() . '"'; ?> ><br /><br />

		    <label for="Name"><b>Name:</b></label>
		    <input type="text" placeholder="Enter product name" name="Name" id="Name" required <?php echo 'value="' . $pModel->getName() . '"'; ?> ><br /><br />

		    <label for="Description"><b>Description:</b></label>
		    <input type="text" placeholder="Enter description" name="Description" id="Description" required <?php echo 'value="' . $pModel->getDescription() . '"'; ?> ><br /><br />

		    <label for="Price"><b>Price:</b></label>
		    <input type="text" placeholder="Enter price" name="Price" id="Price" required <?php echo 'value="' . $pModel->getPrice() . '"'; ?> ><br /><br />

		    <button type="submit">Update</button><br /><br />
		    <hr>
		</form>
	</div>
	</div>

</body>
</html>
