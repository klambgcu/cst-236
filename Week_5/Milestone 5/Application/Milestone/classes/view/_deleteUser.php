<?php
namespace classes\view;
require_once $_SERVER['DOCUMENT_ROOT'] . '/Milestone/AutoLoader.php';
session_start();
?>

<!DOCTYPE html>

<!--
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-10-31
 * Class     : CST-236 Database Application Programming II
 * Professor : Nathan Braun
 * Assignment: Milestone
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Delete User (_deleteUser.php)
 * 2. Simple place holder
 * 3. TO DO: Add card/image/detail
 * ---------------------------------------------------------------
 -->

<html>
<head>
<meta charset="ISO-8859-1">
<link rel=stylesheet href="../../css/main_nav.css" />
<title>STORE NAME HERE</title>
</head>
<body>

<?php require_once('../../util_funcs.php');?>
<?php require_once '../../_main_menu.php';?>
<?php 
$uModel = $user;
$rArray = $roles;
?>



	<div align="center">
    	<hr><br />
    	<h1>Welcome - STORE NAME HERE!</h1>
    	<hr><br />
 	<div align="center">
		<form action="../controller/UserDeleteHandler.php" method="POST">
		    <h1>Delete User Form</h1>
		    <p>Confirm Delete.</p>
		    <hr><br />

			<input type="hidden"  name="UserID" id="UserID" <?php echo 'value="' . $uModel->getId() . '"'; ?> >

		    <label for="FirstName"><b>First Name:</b></label>
		    <input type="text" placeholder="Enter first name" name="FirstName" id="FirstName" disabled <?php echo 'value="' . $uModel->getFirst_name() . '"'; ?> ><br /><br />

		    <label for="LastName"><b>Last Name:</b></label>
		    <input type="text" placeholder="Enter last name" name="LastName" id="LastName" disabled <?php echo 'value="' . $uModel->getLast_name() . '"'; ?> ><br /><br />

		    <label for="Email"><b>Email:</b></label>
		    <input type="email" placeholder="Enter email address" name="Email" id="Email" disabled <?php echo 'value="' . $uModel->getEmail() . '"'; ?> ><br /><br />

		    <label for="Mobile"><b>Mobile:</b></label>
		    <input type="tel" placeholder="Enter mobile number" name="Mobile" id="Mobile" disabled <?php echo 'value="' . $uModel->getMobile() . '"'; ?> ><br /><br />

		    <label for="Password"><b>Password:</b></label>
		    <input type="text" placeholder="Password Length 8 minimum" name="Password" id="Password" pattern=".{8,}" disabled <?php echo 'value="' . $uModel->getPassword() . '"'; ?> ><br /><br />

		    <label for="Birthdate"><b>Birth Date:</b></label>
		    <input type="date" placeholder="Enter birthdate" name="Birthdate" id="Birthdate" disabled <?php echo 'value="' . $uModel->getBirthdate() . '"'; ?> ><br /><br />

<?php

    echo "<label for=\"Gender\"><b>Sex:</b></label>\n";
    echo "<select name=\"Gender\" id=\"Gender\" disabled>\n";
    echo "<option value=\"0\" ";
    if ($uModel->getGender() == 0) echo "selected";
    echo ">Male</option>\n";
    echo "<option value=\"1\" ";
    if ($uModel->getGender() == 1) echo "selected";
    echo ">Female</option>\n";
    echo "</select><br /><br />\n";

    echo "<label for=\"UserRoleID\">Select Role:</label>\n";
    echo "<select name=\"UserRoleID\" id=\"UserRoleID\" disabled>\n";
    for($x=0; $x < count($rArray); $x++)
    {
        echo "<option value=\"" . $rArray[$x]['ID'] . "\"";
        if ($rArray[$x]['ID'] == $uModel->getRole_id())
            echo " selected>";
        else
            echo ">";
            echo $rArray[$x]['ROLENAME'] . "</option>\n";
    }
    echo "</select><br /><br />\n";
?>


		    <button type="submit">Confirm Delete</button><br /><br />
		    <hr>
		</form>
	</div>
	</div>

</body>
</html>
