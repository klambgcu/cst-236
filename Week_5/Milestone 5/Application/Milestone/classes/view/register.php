<?php
namespace classes\view;
require_once $_SERVER['DOCUMENT_ROOT'] . '/Milestone/AutoLoader.php';
?>

<!DOCTYPE html>

<!--
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-10-17
 * Class     : CST-236 Database Application Programming II
 * Professor : Nathan Braun
 * Assignment: Milestone
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Registration Form (register.html)
 * 2. Calls registerHandler.php
 * 3. Added Main Menu requirement
 * ---------------------------------------------------------------
 -->

<html>
<head>
<meta charset="ISO-8859-1">
<link rel=stylesheet href="../../css/main_nav.css" />
<title>Registration Form</title>
</head>
<body>

<?php
require_once '../../_main_menu.php';
?>

	<div align="center">
		<form action="../controller/registerHandler.php" method="POST">
		    <h1>Registration Form</h1>
		    <p>Please fill in this form to create an account.</p>
		    <hr><br />

		    <label for="FirstName"><b>First Name:</b></label>
		    <input type="text" placeholder="Enter first name" name="FirstName" id="FirstName" required><br /><br />

		    <label for="LastName"><b>Last Name:</b></label>
		    <input type="text" placeholder="Enter last name" name="LastName" id="LastName" required><br /><br />

		    <label for="Email"><b>Email:</b></label>
		    <input type="email" placeholder="Enter email address" name="Email" id="Email" required><br /><br />

		    <label for="Mobile"><b>Mobile:</b></label>
		    <input type="tel" placeholder="Enter mobile number" name="Mobile" id="Mobile" required><br /><br />

		    <label for="Password"><b>Password:</b></label>
		    <input type="password" placeholder="Password Length 8 minimum" name="Password" id="Password" pattern=".{8,}" required><br /><br />

		    <label for="Birthdate"><b>Birth Date:</b></label>
		    <input type="date" placeholder="Enter birthdate" name="Birthdate" id="Birthdate" required><br /><br />

		    <label for="Gender"><b>Sex:</b></label>
		    <select name="Gender" id="Gender" required>
				<option value="0">Male</option>
				<option value="1">Female</option>
			</select><br /><br />

		    <button type="submit">Register</button><br /><br />
		    <hr>
		</form>
	</div>

</body>
</html>