<?php
namespace classes\view;
require_once $_SERVER['DOCUMENT_ROOT'] . '/Milestone/AutoLoader.php';
session_start();
?>

<!DOCTYPE html>

<!--
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-10-31
 * Class     : CST-236 Database Application Programming II
 * Professor : Nathan Braun
 * Assignment: Milestone
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. View/Display one Product
 * 2. Simple place holder
 * 3. TO DO: Add card/image/detail
 * ---------------------------------------------------------------
 -->

<html>
<head>
<meta charset="ISO-8859-1">
<link rel=stylesheet href="../../css/main_nav.css" />
<title>STORE NAME HERE</title>
</head>
<body>

<?php require_once('../../util_funcs.php');?>
<?php require_once '../../_main_menu.php';?>
<?php $pModel = $product; ?>



	<div align="center">
    	<hr><br />
    	<h1>Welcome - STORE NAME HERE!</h1>
    	<hr><br />
 	<div align="center">
		<form action="../controller/ShoppingCartChangeHandler.php" method="POST">
		    <h1>View Product Form</h1>
		    <hr><br />

			<img name="ProductImage" id="ProductImage" src="../../images/<?php echo $pModel->getImage() ?> " <?php echo 'value="' . $pModel->getImage() . '"'; ?> ><br /><br />
			<input type="hidden"  name="Image" id="Image" <?php echo 'value="' . $pModel->getImage() . '"'; ?> >
			<input type="hidden"  name="ProductID" id="ProductID" <?php echo 'value="' . $pModel->getId() . '"'; ?> >

		    <label for="ScanCode"><b>Scan Code:</b></label>
		    <input type="text" placeholder="Enter scan code" name="ScanCode" id="ScanCode" disabled <?php echo 'value="' . $pModel->getScancode() . '"'; ?> ><br /><br />

		    <label for="Name"><b>Name:</b></label>
		    <input type="text" placeholder="Enter product name" name="Name" id="Name" disabled <?php echo 'value="' . $pModel->getName() . '"'; ?> ><br /><br />

		    <label for="Description"><b>Description:</b></label>
		    <input type="text" placeholder="Enter description" name="Description" id="Description" disabled <?php echo 'value="' . $pModel->getDescription() . '"'; ?> ><br /><br />

		    <label for="Price"><b>Price:</b></label>
		    <input type="text" placeholder="Enter price" name="Price" id="Price" disabled <?php echo 'value="' . $pModel->getPrice() . '"'; ?> ><br /><br />

		    <label for="Qty"><b>Qty:</b></label>
		    <select name="Qty" id="Qty" required>
				<option value="1" selected>1</option>
				<option value="2">2</option>
				<option value="3">3</option>
				<option value="4">4</option>
				<option value="5">5</option>
				<option value="6">6</option>
				<option value="7">7</option>
				<option value="8">8</option>
				<option value="9">9</option>
				<option value="10">10</option>
			</select><br /><br />
			
			<input type='hidden' name='Mode' id='Mode' value='0'>

		    <button type="submit">Add to Cart</button><br /><br />
		    <hr>
		</form>
	</div>
	</div>

</body>
</html>
