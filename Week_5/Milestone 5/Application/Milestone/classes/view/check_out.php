<?php
namespace classes\view;
use classes;

require_once $_SERVER['DOCUMENT_ROOT'] . '/Milestone/AutoLoader.php';

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-11-13
 * Class     : CST-236 Database Application Programming II
 * Professor : Nathan Braun
 * Assignment: Milestone
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Milestone - Handle Check Out Process
 * 2. Obtain form data
 * 3. Handle Data / Display
 * ---------------------------------------------------------------
 */

include_once '../../header.php';
include_once '../../securePage.php';
require_once '../../util_funcs.php';
?>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<link rel=stylesheet href="../../css/main_nav.css" />
<link rel=stylesheet href="../../css/post_entries.css" />
<title>STORE NAME HERE</title>
</head>
<body>

<?php require_once '../../_main_menu.php';?>

<div align="center">
	<hr><br />
	<h1>Welcome - STORE NAME HERE!</h1>
	<hr><br />
	<h1>Order Check Out</h1><br />
	<form action="../controller/CheckOutHandler.php" method="POST">
        <br />
        <hr>
        <h3>Step 1. Shipping Address</h3>
        <br /><br />
    
        <label for="S_Street"><b>Street:</b></label>
        <input type="text" placeholder="Street Address" name="S_Street" id="S_Street" required><br /><br />
    
        <label for="S_City"><b>City:</b></label>
        <input type="text" placeholder="City" name="S_City" id="S_City" required><br /><br />
    
        <label for="S_State"><b>State:</b></label>
        <input type="text" placeholder="State Abbr. XX" name="S_State" id="S_State"  pattern="[A-Z][A-Z]" required><br /><br />
    
        <label for="S_Postal"><b>Zip Code:</b></label>
        <input type="text" placeholder="Postal Code" name="S_Postal" id="S_Postal" required><br /><br />
    	<br /><hr>
        <h3>Step 2. Billing Address</h3>
        <br /><br />
    
        <label for="B_Street"><b>Street:</b></label>
        <input type="text" placeholder="Street Address" name="B_Street" id="B_Street" required><br /><br />
    
        <label for="B_City"><b>City:</b></label>
        <input type="text" placeholder="City" name="B_City" id="B_City" required><br /><br />
    
        <label for="B_State"><b>State:</b></label>
        <input type="text" placeholder="State Abbr. XX" name="B_State" id="B_State" pattern="[A-Z][A-Z]" required><br /><br />
    
        <label for="B_Postal"><b>Zip Code:</b></label>
        <input type="text" placeholder="Postal Code" name="B_Postal" id="B_Postal" required><br /><br />
    
        <br /><hr>
        <h3>Step 3. Credit Card Information</h3>
        <br><br>
        
        <label for="CC_Name"><b>Name on Card:</b></label>
        <input type="text" placeholder="Name on credit card" name="CC_Name" id="CC_Name" required><br /><br />

        <label for="CC_Number"><b>Number:</b></label>
        <input type="text" placeholder="9999 9999 9999 9999" name="CC_Number" id="CC_Number" pattern="[0-9][0-9][0-9][0-9] [0-9][0-9][0-9][0-9] [0-9][0-9][0-9][0-9] [0-9][0-9][0-9][0-9]" required><br /><br />

        <label for="CC_Expiration"><b>Expiration:</b></label>
        <input type="text" placeholder="MM/DD" name="CC_Expiration" id="CC_Expiration" pattern="[0-9][0-9]/[0-9][0-9]" required><br /><br />
       
        <label for="CC_Security"><b>CVV:</b></label>
        <input type="text" placeholder="Security Code" name="CC_Security" id="CC_Security" pattern="[0-9][0-9][0-9]" required><br /><br />     
        
        <br /><hr>
        <h3>Step 4. Review Order</h3>
        <br><br>
        
        <table id="post_entries">
  		  <thead>
    		<tr>
                <th>Name</th>
                <th>Price</th>
                <th>Qty</th>
                <th>Ext. Cost</th>
            </tr>
          </thead>
          <tbody>

        <?php 
        // Get Logged in user information to display cart summary
        $user_info = getUserInfo();
        $user_id = $user_info[0]["ID"];
        $scService = new classes\business\ShoppingCartBusinessService();
        $cart_list = $scService->getCart($user_id);
        $cart_cost = $scService->getCartTotalCost($user_id);
        
        foreach($cart_list as $item)
        {
            echo "  <tr>\n";
            echo "      <td>" .$item->getName() . "</td>\n";
            echo "      <td>" . "$ " . $item->getPrice() . "</td>\n";
            echo "      <td>" . $item->getQty() . "</td>\n";
            echo "      <td>" . "$ " . $item->getExtended_cost() . "</td>\n";
            echo "  </tr>\n";
        }
        
        ?>

          </tbody>
        </table>

        <?php 
        echo "<hr><br><div align='center'><h3>Total: $" . $cart_cost . "</h3></div>";
        ?>

        <br /><hr>
        <h3>Step 5. Confirm Order</h3>
        <br><br>
        
        Confirm that you have entered correct information above 
        <input type="checkbox" id="Confirm" name="Confirm" required ><br /><br />
        
        <button type="submit">Submit Order</button><br /><br />
        <br /><hr>
	</form>
</div>
</body>
</html>