<?php 
namespace classes\view;
require_once $_SERVER['DOCUMENT_ROOT'] . '/Milestone/AutoLoader.php';
include_once '../../header.php';
include_once '../../securePage.php';
?>

<!DOCTYPE html>

<!--
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-10-24
 * Class     : CST-236 Database Application Programming II
 * Professor : Nathan Braun
 * Assignment: Milestone
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Product Catalog Page (Initial Page)
 * 2. 
 * 3.
 * ---------------------------------------------------------------
 -->

<html>
<head>
<meta charset="ISO-8859-1">
<link rel=stylesheet href="../../css/main_nav.css" />
<link rel=stylesheet href="../../css/post_entries.css" />
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.3/css/jquery.dataTables.css"> 
<script
  src="https://code.jquery.com/jquery-3.6.0.slim.min.js"
  integrity="sha256-u7e5khyithlIdTpu22PHhENmPcRdFiHRjhAuHcs05RI="
  crossorigin="anonymous">
  </script>

<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.js"></script>


<title>STORE NAME HERE</title>
</head>
<body>

<?php 
require_once '../../util_funcs.php';
require_once '../../_main_menu.php';
?>

	<div align="center">
    	<hr><br />
    	<h1>Product Catalog - STORE NAME HERE!</h1>
    	<hr><br />
	</div>

	<div align="center">
		<form action="../controller/ProductSearchHandler.php" method="POST">
		    <h1>Product Search</h1>
		    <p>Enter Search Criteria - Leave blank for all products.</p>
		    <hr><br />

		    <label for="SearchPattern"><b>Product Name:</b></label>
		    <input type="text" placeholder="Enter search criteria" name="SearchPattern" id="SearchPattern" ><br /><br />

		    <button type="submit">Search</button><br /><br />
		    <hr>
		</form>
	</div>

<?php
    include ('../view/_displayAllProducts.php');
?>

<script>
$(document).ready( function () {
	$('#post_entries').DataTable();
} );
</script>

</body>
</html>
