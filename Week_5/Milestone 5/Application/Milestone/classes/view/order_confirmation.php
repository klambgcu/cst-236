<?php
namespace classes\view;
use classes;

require_once $_SERVER['DOCUMENT_ROOT'] . '/Milestone/AutoLoader.php';
?>

<!--
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-11-14
 * Class     : CST-236 Database Application Programming II
 * Professor : Nathan Braun
 * Assignment: Milestone
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Milestone display order confirmation
 * 2. Reusable functions
 * ---------------------------------------------------------------
 -->
 
<html>
<head>
<meta charset="ISO-8859-1">
<link rel=stylesheet href="../../css/main_nav.css" />
<title>Order Confirmation</title>
<style>
table, td, th {
  border: 1px solid black;
}

table {
  width: 80%;
  border-collapse: collapse;
}
</style>
</head>
<body>

<?php
require_once '../../_main_menu.php';
?>

<div align="center" style="background-color: white;">
<br /><br /><hr><br /><h1>Order Confirmation</h1>
<br /><hr><br /><br />

<table>
<thead>
<tr>
<th width="40%">Billing Address:</th>
<th width="40%">Shipping Address:</th>
<th width="20%">Order Information:</th>
</tr>
</thead>
<tbody>
<tr>
<?php 
$ba = $billAddress;
$sa = $shipAddress;


echo "<td>\n";
echo $user_info[0]["FIRST_NAME"] . " " . $user_info[0]["LAST_NAME"] . "<br />\n";
echo $ba->getStreet() . "<br />\n";
echo $ba->getCity() . "<br />\n";
echo $ba->getState()  . "<br />\n";
echo $ba->getPostal()  . "<br />\n";
echo "</td>\n";

echo "<td>\n";
echo $user_info[0]["FIRST_NAME"] . " " . $user_info[0]["LAST_NAME"] . "<br />\n";
echo $sa->getStreet() . "<br />\n";
echo $sa->getCity() . "<br />\n";
echo $sa->getState()  . "<br />\n";
echo $sa->getPostal()  . "<br />\n";
echo "</td>\n";

echo "<td>\n";
echo "Date: " . date("Y-m-d")  . "<br />\n";
echo "Order Number: " . $order_id   . "<br />\n";
echo "Payment: Card Ending: " . substr($creditCard->getNumber(),-4) . "<br />\n";

echo "</td>\n";

?>
</tr>
</tbody>
</table>
<br /><br />

<table>
  <thead>
    <tr>
        <th>Line</th>
        <th>Product ID</th>
        <th>Name</th>
        <th>Price</th>
        <th>Qty</th>
        <th>Ext. Cost</th>
    </tr>
  </thead>
  <tbody>
<?php

require_once '../../util_funcs.php';

$line = 1;
foreach($cart_list as $p) 
{

    echo "  <tr>\n";
    echo "      <td align='center'>" . $line . "</td>\n";
    echo "      <td>" . $p->getProduct_id() . "</td>\n";
    echo "      <td>" . $p->getName() . "</td>\n";
    echo "      <td align='right'>" . "$ " . $p->getPrice() . "</td>\n";
    echo "      <td align='middle'>" . $p->getQty() . "</td>\n";
    echo "      <td align='right'>" . "$ " . $p->getExtended_cost() . "</td>\n";
    echo "  </tr>\n";
    $line = $line + 1;
}

 ?>
  <tr>
  <td colspan="5">Total</td>
  <?php echo "<td align='right'>" . "$ " . $cart_cost  . "</td>\n"; ?>
  </tr>
  </tbody>
</table>
<br />
<br />
<hr>
<br />
<br />
</div>