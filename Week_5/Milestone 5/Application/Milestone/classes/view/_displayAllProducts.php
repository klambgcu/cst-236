<?php
namespace classes\view;
require_once $_SERVER['DOCUMENT_ROOT'] . '/Milestone/AutoLoader.php';
?>

<!--
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-10-24
 * Class     : CST-236 Database Application Programming II
 * Professor : Nathan Braun
 * Assignment: Milestone
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Milestone (_displayAllProduct.php) display table of products
 * 2. Reusable functions
 * ---------------------------------------------------------------
 -->

<table id="post_entries" class="display">
  <thead>
    <tr>
        <th>Action</th>
        <th>ID</th>
        <th>Scan Code</th>
        <th>Name</th>
        <th>Description</th>
        <th>Price</th>
    </tr>
  </thead>
  <tbody>
<?php

require_once '../../util_funcs.php';

$products = getSearchInfo();

if (isset($products))
{
    foreach($products as $p) 
    {
        echo "  <tr>\n";
        echo "<td><form action='../controller/ShoppingCartChangeHandler.php' method='POST'>\n" .
             "<input type='hidden' name='ProductID' id='ProductID' value='" . $p[0] . "'>\n" .
             "<input type='hidden' name='Mode' id='Mode' value='0'>\n" .
             "<input type='hidden' name='Qty' id='Qty' value='1'>\n" .
             "<button type='submit'>Add Cart</button></form></td>\n";
        echo "      <td>" . $p[0] . "</td>\n";
        echo "      <td><a href='../controller/ProductDisplayHandler.php?id=" . $p[0] . "'>" . $p[1] . "</a></td>\n";
        echo "      <td>" . $p[2] . "</td>\n";
        echo "      <td>" . $p[3] . "</td>\n";
        echo "      <td>" . $p[4] . "</td>\n";
        echo "  </tr>\n";
	}
}
	
	
 ?>

  </tbody>
</table>
