<?php
namespace classes\business;
use classes;

require_once $_SERVER['DOCUMENT_ROOT'] . '/Milestone/AutoLoader.php';

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-10-22
 * Class     : CST-236 Database Application Programming II
 * Professor : Nathan Braun
 * Assignment: Milestone
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Milestone
 * 2. Business Layer
 * 3. Product information
 * ---------------------------------------------------------------
 */

class ProductBusinessService
{

    public function __construct()
    {}
    
    public function searchByProductName($pattern)
    {
        $service = new classes\database\ProductDataService();
        return $service->findByProductName($pattern);
    }

    public function getAllProducts()
    {
        $service = new classes\database\ProductDataService();
        return $service->getAllProducts();
    }

    public function getProductById($id)
    {
        $service = new classes\database\ProductDataService();
        return $service->getProductById($id);
    }
    
    public function deleteProductById($id)
    {
        $service = new classes\database\ProductDataService();
        return $service->delete($id);
    }
    
    public function updateProduct($product)
    {
        $service = new classes\database\ProductDataService();
        return $service->update($product);
    }
        
    public function createProduct($product)
    {
        $service = new classes\database\ProductDataService();
        return $service->create($product);
    }
    
}

