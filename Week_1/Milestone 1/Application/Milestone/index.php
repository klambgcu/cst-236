<!DOCTYPE html>

<!--
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-10-17
 * Class     : CST-236 Database Application Programming II
 * Professor : Nathan Braun
 * Assignment: Milestone
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Welcome / Default Page (index.html)
 * 2. Initial Menu Options for home, login, register
 * 3.
 * ---------------------------------------------------------------
 -->

<html>
<head>
<meta charset="ISO-8859-1">
<link rel=stylesheet href="css/main_nav.css" />
<link rel=stylesheet href="css/post_entries.css" />
<title>STORE NAME HERE</title>
</head>
<body>

<?php require_once 'util_funcs.php' ?>
<?php require_once '_main_menu.php';?>

	<div align="center">
    	<hr><br />
    	<h1>Welcome - STORE NAME HERE!</h1>
    	<hr><br />
	</div>
<?php
    // Check if user is logged in - display blogs
    $user_info = getUserInfo();

    if (isset($user_info))
    {
        $search_info = getSearchInfo();

        echo "        <div align=\"center\">\n";
        echo "                <h3>Login Successful</h3><br />\n";
        echo "                <hr><br />\n";
        echo "                Welcome: " .  $user_info[0]["FIRST_NAME"] . " " . $user_info[0]["LAST_NAME"] . "<br />\n";
        echo "                <br />\n";
        echo "                <hr>\n";
        echo "        </div>\n";
    }
?>

</body>
</html>