<?php

/*
<!----------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-10-17
 * Class     : CST-236 Database Application Programming II
 * Professor : Nathan Braun
 * Assignment: Milestone
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Login Failure form
 * 2. Obtain form data
 * 3. Handle Login Validation
 * 4. Transistion from procedural connection to PDO object
 * ---------------------------------------------------------------
 */

?>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<link rel=stylesheet href="css/main_nav.css" />
<link rel=stylesheet href="css/post_entries.css" />
<title>STORE NAME HERE</title>
</head>
<body>

<?php require_once 'util_funcs.php' ?>
<?php require_once '_main_menu.php';?>

	<div align="center">
    	<hr><br />
    	<h1>Welcome - STORE NAME HERE!</h1>
    	<hr><br />
	</div>
<?php
        echo "        <div align=\"center\">\n";
        echo "                <h3>Login Was Not Successful</h3><br />\n";
        echo "                <hr><br />\n";
        echo "                Login not successful - please try again<br />\n";
        echo "                <br />\n";
        echo "                <hr>\n";
        echo "        </div>\n";
?>

</body>
</html>