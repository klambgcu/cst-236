<?php require_once 'util_funcs.php' ?>

<!--
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-10-17
 * Class     : CST-236 Database Application Programming II
 * Professor : Nathan Braun
 * Assignment: Milestone
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Main Menu (_main_menu.php)
 * 2.
 * 3.
 * ---------------------------------------------------------------
 -->


<nav id="main_nav">
<?php
$user_info = getUserInfo();

if (isset($user_info))
{
    $menu_name = "(" . $user_info[0]["FIRST_NAME"] . " " . $user_info[0]["LAST_NAME"] . ")";

    //
    // User is logged into the application
    //
    echo "  <ul>";
    echo "      <li><a href=\"index.php\">Home</a></li>";
    echo "      <li>";
    echo "          <a href=\"\">Account $menu_name &#9660;</a>";
    echo "          <ul>";
    echo "              <li><a href=\"register.php\">Registration...</a></li>";
    echo "              <li><a href=\"logoutHandler.php\">Log Out</a></li>";
    echo "          </ul>";
    echo "      </li>";
    echo "  </ul>";
}
else
{
    //
    // User is NOT logged into the application
    //
    echo "  <ul>";
    echo "      <li><a href=\"index.php\">Home</a></li>";
    echo "      <li>";
    echo "          <a href=\"\">Account &#9660;</a>";
    echo "          <ul>";
    echo "              <li><a href=\"register.php\">Registration...</a></li>";
    echo "              <li><a href=\"login.php\">Login...</a></li>";
    echo "          </ul>";
    echo "      </li>";
    echo "  </ul>";
}
?>

</nav>

