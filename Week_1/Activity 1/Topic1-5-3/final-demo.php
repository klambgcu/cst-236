<?php

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-10-17
 * Class     : CST-236 Database Application Programming II
 * Professor : Nathan Braun
 * Assignment: Activity 1.5 (final)
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Activity 1.5.3 (final)
 * 2. Object Oriented Programming - Person
 * 3. Final Testing Script
 * ---------------------------------------------------------------
 */

require_once 'Person.php';
require_once 'Student.php';

$newGuy1 = new Person();
$newGuy2 = new Student();

$newGuy1->growOlderBy(2);
$newGuy2->growOlderBy(2);

echo "The age of person 1 is " . $newGuy1->age . ".<br />";
echo "The age of person 2 is " . $newGuy2->age . ".<br />";

?>
