<?php

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-10-17
 * Class     : CST-236 Database Application Programming II
 * Professor : Nathan Braun
 * Assignment: Activity 1.5 (final)
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Activity 1.5.3 (final)
 * 2. Object Oriented Programming - Person
 * 3. Student Class Final
 * ---------------------------------------------------------------
 */

require_once 'Person.php';

class Student extends Person
{

    public function growOlderBy($decade)
    {
        $this->age = $this->age + (10 * $decade);
    }
    
}

?>
