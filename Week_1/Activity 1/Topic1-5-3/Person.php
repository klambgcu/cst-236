<?php

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-10-17
 * Class     : CST-236 Database Application Programming II
 * Professor : Nathan Braun
 * Assignment: Activity 1.5 (final)
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Activity 1.5.3 (final)
 * 2. Object Oriented Programming - Person
 * 3. Person Class Final
 * ---------------------------------------------------------------
 */

// Keyword final here says no class can extend this class
class Person
{
    public $name;
    public $age;
    
    // Keyword final here says no class can override the method.
    final public function growOlderBy($year)
    {
        $this->age = $this->age + $year;
    }
}

