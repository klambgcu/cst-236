<?php

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-10-17
 * Class     : CST-236 Database Application Programming II
 * Professor : Nathan Braun
 * Assignment: Activity 1.5 (abstract)
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Activity 1.5.2 (abstract)
 * 2. Object Oriented Programming - Animal
 * 3. Animal Class Abstract
 * ---------------------------------------------------------------
 */

abstract class Animal
{
    public $name;
    public $color;
    
    public function __construct($n, $c)
    {
        $this->name = $n;
        $this->color = $c;
    }

    abstract function talk();
    
    abstract function doTrick();

}

?>
