<?php

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-10-17
 * Class     : CST-236 Database Application Programming II
 * Professor : Nathan Braun
 * Assignment: Activity 1.5 (abstract)
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Activity 1.5.2 (abstract)
 * 2. Object Oriented Programming - Animal
 * 3. Animal Testing Script
 * ---------------------------------------------------------------
 */

require_once 'Dog.php';
require_once 'Cat.php';

$cat1 = new Cat("Morris", "Orange");
$dog1 = new Dog("Fido", "Black");

// This cannot be done - error because Animal is abstract and cannot be instantiated.
// $beast = new Animal("Harry", "Green");

$cat1->talk();
$dog1->talk();

$cat1->doTrick();
$dog1->doTrick();

?>
