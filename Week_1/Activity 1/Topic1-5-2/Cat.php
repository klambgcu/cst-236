<?php

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-10-17
 * Class     : CST-236 Database Application Programming II
 * Professor : Nathan Braun
 * Assignment: Activity 1.5 (abstract)
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Activity 1.5.2 (abstract)
 * 2. Object Oriented Programming - Animal
 * 3. Cat Class
 * ---------------------------------------------------------------
 */

require_once 'Animal.php';

class Cat extends Animal
{
    public function talk()
    {
        echo "Meow<br />";
    }
    public function doTrick()
    {
        echo "You gotta be kidding me.<br />";
    }



}

