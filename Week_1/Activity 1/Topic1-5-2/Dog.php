<?php

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-10-17
 * Class     : CST-236 Database Application Programming II
 * Professor : Nathan Braun
 * Assignment: Activity 1.5 (abstract)
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Activity 1.5.2 (abstract)
 * 2. Object Oriented Programming - Animal
 * 3. Dog Class 
 * ---------------------------------------------------------------
 */

require_once 'Animal.php';

class Dog extends Animal
{

    public function talk()
    {
        echo "bark bark<br />";
    }
    public function doTrick()
    {
        echo "Jumps, fetches, and anything else you like.<br />";
    }


}

?>
