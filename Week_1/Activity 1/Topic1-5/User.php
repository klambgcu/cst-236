<?php

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-10-17
 * Class     : CST-236 Database Application Programming II
 * Professor : Nathan Braun
 * Assignment: Activity 1.5 (static)
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Activity 1.5 (static)
 * 2. Object Oriented Programming - User
 * 3. User Class - static fields / methods
 * ---------------------------------------------------------------
 */


class User
{

    public $username;
    public $password;
    
    public static $minPassLength = 5;
    
    public static function validatePassword($password)
    {
        if (strlen($password) > self::$minPassLength)
        {
            return true;
        }
        else 
        {
            return false;
        }
    }
}

?>
