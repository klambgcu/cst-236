<?php

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-10-17
 * Class     : CST-236 Database Application Programming II
 * Professor : Nathan Braun
 * Assignment: Activity 1.5 (static)
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Activity 1.5 (static)
 * 2. Object Oriented Programming - User
 * 3. User - static field / method test
 * ---------------------------------------------------------------
 */

require_once 'User.php';

$pw1 = 'asdf';
$pw2 = 'asdsdfsfsfdgdfgdgdadgag4242f';

echo "Try pw1 with length of 4.<br />";

if (User::validatePassword($pw1))
{
    echo "Password is long enough!<br />";
}
else 
{
    echo "Your password is too short.<br />";
}

echo "Try pw2 with length of 28.<br />";


if (User::validatePassword($pw2))
{
    echo "Password is long enough!<br />";
}
else
{
    echo "Your password is too short.<br />";
}




?>
