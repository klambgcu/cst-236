<?php

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-10-17
 * Class     : CST-236 Database Application Programming II
 * Professor : Nathan Braun
 * Assignment: Activity 1.3
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Activity 1.3
 * 2. Object Oriented Programming - Driver
 * 3. race car test driver 
 * ---------------------------------------------------------------
 */

require_once "Car.php";

// Create a new race car
$raceCar = new Car();

// Setup the car
$raceCar->installEngine();
$raceCar->addTires(4);
$raceCar->inflateTiresTo(32);

// Start and drive the car
$raceCar->start();
$raceCar->setSpeed(60);

// Stop and then restart the car
$raceCar->stop();
$raceCar->restart();

?>
