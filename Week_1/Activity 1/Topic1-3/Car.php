<?php

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-10-17
 * Class     : CST-236 Database Application Programming II
 * Professor : Nathan Braun
 * Assignment: Activity 1.3
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Activity 1.3
 * 2. Object Oriented Programming - Car (Race Car)
 * 3. Car Class (Race Car)
 * ---------------------------------------------------------------
 */

class Car
{
    private $tires;
    private $hasEngine;
    private $tirePressure;
    private $isRunning;
    private $speed;
    
    public function __construct()
    {
        $this->tires = 0;
        $this->hasEngine = false;
        $this->tirePressure = 0;
        $this->isRunning = false;
        $this->speed = 0;
    }

    public function addTires($numberOfNewTires)
    {
        if ($numberOfNewTires > 0 && $numberOfNewTires <= 4)
        {
            if ($this->tires + $numberOfNewTires > 4)
            {
                echo "You can have a maximum of four tires. Try again.<br />";
            }
            else 
            {
                $this->tires = $this->tires + $numberOfNewTires;
                echo "You installed " . $numberOfNewTires . " new tires. You now have " . $this->tires . " tires installed on your car.<br />";
            }
        }
        else 
        {
            echo $numberOfNewTires . " is an invalid number of tires. Must be four or less.<br />";
        }
    }
    
    public function inflateTiresTo($newTirePressure)
    {
        if ($this->tires > 0)
        {
            if ($newTirePressure >= 0 && $newTirePressure <= 40)
            {
                $this->tirePressure = $newTirePressure;
                echo "Each of the " . $this->tires . " tires has been inflated to " . $newTirePressure . ".<br />";
            }
            else 
            {
                echo $newTirePressure . " is an invalid tire pressure.<br />";
            }
        }
        else 
        {
            echo "There are no tires installed on the car to inflate.<br />";
        }
    }
    
    public function installEngine()
    {
        $this->hasEngine = true;
        echo "You have installed an engine in the car.<br />";
    }
    
    public function restart()
    {
        if ($this->isRunning == false)
        {
            echo "Attempting to restart the car.<br />";
            $this->start();
        }
    }
    
    public function setSpeed($newSpeed)
    {
        if ($this->isRunning)
        {
            if ($newSpeed >= 0 && $newSpeed <= 60)
            {
                $this->speed = $newSpeed;
                echo "Speed has changed to " . $this->speed . ".<br />";
            }
            else 
            {
                echo $newSpeed . " is an invalid speed.<br />";
            }
        }
        else
        {
            echo "The car is not started - please start car first.<br />";
        }
    }
    
    public function start()
    {
        if ($this->isRunning == false && $this->hasEngine && $this->tirePressure >= 32 && $this->tires == 4)
        {
            $this->isRunning = true;
            echo "The car is started.<br />";
        }
    }
    
    public function stop()
    {
        echo "Applying the brakes.<br />";
        $this->setSpeed(0);
        echo "Turning the car off.<br />";
        $this->isRunning = false;
    }
}

?>


