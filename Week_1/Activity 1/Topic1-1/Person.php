<?php

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-10-17
 * Class     : CST-236 Database Application Programming II
 * Professor : Nathan Braun
 * Assignment: Activity 1.2
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Activity 1.2
 * 2. Object Oriented Programming - Person
 * 3. Person Class (People)
 * ---------------------------------------------------------------
 */


class Person
{
    // Properties
    private $name;

    // Constructor for creating a new person
    public function __construct($name)
    {
        echo "Hello, my name is " . $name . ".<br/>";
        $this->name = $name;
        $this->username = "shad";
        $this->password = "password";
    }
    
    // Methods - What can a person do
    public function walk()
    {
        echo "I am walking.....<br />";
    }
    
    public function formalGreeting()
    {
        echo "Good day to you sir. You can address me as " . $this->name . ".<br />";
    }
    
    public function spanishGreeting()
    {
        echo "Hola. Me llamo " . $this->name . ".<br />";
    }
    
    public function login($a, $b)
    {
        if ($a == $this->username && $b == $this->password)
        {
            echo $this->name . " has been logged in successfully.<br />";
        }
        else 
        {
            echo "Login failed. That doesn't seem quite right.<br />";
        }
    }
}

?>
