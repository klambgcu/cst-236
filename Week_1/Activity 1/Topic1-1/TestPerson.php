<?php

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-10-17
 * Class     : CST-236 Database Application Programming II
 * Professor : Nathan Braun
 * Assignment: Activity 1.2
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Activity 1.2
 * 2. Object Oriented Programming - Person
 * 3. Person Test Script
 * ---------------------------------------------------------------
 */


// require_once "Autoloader.php";
require_once "Person.php";

// Create an instance of a Person
$person = new Person("Mark");
$person->walk();

$person2 = new Person("Edward");
$person2->formalGreeting();

$person3 = new Person("Juan");
$person3->spanishGreeting();

$person3->login("shad", "asdf");
$person3->login("shad", "password");    


?>
