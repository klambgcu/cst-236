<?php

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-10-17
 * Class     : CST-236 Database Application Programming II
 * Professor : Nathan Braun
 * Assignment: Activity 1.4
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Activity 1.4
 * 2. Object Oriented Programming - SuperHero
 * 3. One of the super heroes
 * ---------------------------------------------------------------
 */

require_once 'SuperHero.php';

class Batman extends SuperHero
{
    function __construct()
    {
        // Call Base Constructor with name and random health 1-1000.
        parent::__construct("Batman", rand(1,1000));
    }
}

