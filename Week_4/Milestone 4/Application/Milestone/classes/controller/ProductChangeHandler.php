<?php
namespace classes\controller;
use classes;
include_once $_SERVER['DOCUMENT_ROOT'] . '/Milestone/AutoLoader.php';

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-10-31
 * Class     : CST-236 Database Application Programming II
 * Professor : Nathan Braun
 * Assignment: Milestone
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Milestone - Handle Post Edit / Delete Requests
 * 2. Obtain form data
 * 3. Handle Login Validation
 * ---------------------------------------------------------------
 */

require_once('../../util_funcs.php');

// store registration parameters
$product_id = filter_input(INPUT_GET,'id');
$mode       = filter_input(INPUT_GET,'mode'); // 0 - Edit, 1 - Delete (Disable for now)

// Validate mode operations
if ( ($mode < 0) || ($mode > 1) )
{
    echo "Invalid Request Operation - Contact Administrator.<br />";
    exit();
}

$ps = new classes\business\ProductBusinessService();
$product = $ps->getProductById($product_id);

if ($mode == 0)
{
    include('../view/_editProduct.php');
}
else
{
    include('../view/_deleteProduct.php');
}
?>