<?php
namespace classes\controller;
use classes;

include_once $_SERVER['DOCUMENT_ROOT'] . '/Milestone/AutoLoader.php';

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-11-07
 * Class     : CST-236 Database Application Programming II
 * Professor : Nathan Braun
 * Assignment: Milestone
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Milestone - Handle Shopping Cart
 * 2. Obtain form data
 * 3. Handle Data / Display
 * ---------------------------------------------------------------
 */

include_once '../../header.php';
include_once '../../securePage.php';
require_once '../../util_funcs.php';

// Get criteria from product catalog search
$product_id = filter_input(INPUT_POST, "ProductID");
$mode = filter_input(INPUT_POST, "Mode");

// Insert into cart - qty 1
if ($mode == 0)
{
    $service = new classes\business\ShoppingCartBusinessService();
    $user_info = getUserInfo();
    $user_id = $user_info[0]["ID"];
    $qty = 1;
    $service->addToCart($user_id, $product_id, $qty);
    
    $list = $service->getCart($user_id);
    echo "<pre>";
    print_r($list);
    echo "</pre>";
    
    
}



// Redirect to display data
// header('Location: ../view/productCatalog.php');

?>