<?php
namespace classes\controller;
use classes;

include_once $_SERVER['DOCUMENT_ROOT'] . '/Milestone/AutoLoader.php';

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-11-07
 * Class     : CST-236 Database Application Programming II
 * Professor : Nathan Braun
 * Assignment: Milestone
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Milestone - Handle Shopping Cart
 * 2. Obtain form data
 * 3. Handle Data / Display
 * ---------------------------------------------------------------
 */

include_once '../../header.php';
include_once '../../securePage.php';
require_once '../../util_funcs.php';
?>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<link rel=stylesheet href="../../css/main_nav.css" />
<link rel=stylesheet href="../../css/post_entries.css" />
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.3/css/jquery.dataTables.css">
<script
  src="https://code.jquery.com/jquery-3.6.0.slim.min.js"
  integrity="sha256-u7e5khyithlIdTpu22PHhENmPcRdFiHRjhAuHcs05RI="
  crossorigin="anonymous">
  </script>

<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.js"></script>
<title>STORE NAME HERE</title>
</head>
<body>

<?php require_once '../../_main_menu.php';?>

	<div align="center">
    	<hr><br />
    	<h1>Welcome - STORE NAME HERE!</h1>
    	<hr><br />
    	<h1>Shopping Cart</h1><br />
	</div>

<br />

<?php
    $user_info = getUserInfo();
    $user_id = $user_info[0]["ID"];

    $service = new classes\business\ShoppingCartBusinessService();
    $cart_list = $service->getCart($user_id);
    include('../view/_displayShoppingCart.php');
?>

<script>
$(document).ready( function () {
	$('#post_entries').DataTable();
} );
</script>

</body>
</html>