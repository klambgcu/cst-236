<?php
namespace classes\controller;
use classes;

include_once $_SERVER['DOCUMENT_ROOT'] . '/Milestone/AutoLoader.php';

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-10-31
 * Class     : CST-236 Database Application Programming II
 * Professor : Nathan Braun
 * Assignment: Milestone
 * Disclaimer: This is my own work
* ---------------------------------------------------------------
* Description:
* 1. Edit User Handler (UserEditHandler.php)
* 2. Retrieves fields from _editUser.php
* 3. Stores in database
* ---------------------------------------------------------------
*/

require_once('../../util_funcs.php');

// store registration parameters
$product_id  = filter_input(INPUT_POST,'ProductID');
// $scancode    = filter_input(INPUT_POST,'ScanCode');
// $name        = filter_input(INPUT_POST,'Name');
// $description = filter_input(INPUT_POST,'Description');
// $price       = filter_input(INPUT_POST,'Price');

$service = new classes\business\ProductBusinessService();
$result = $service->deleteProductById($product_id);

if (! $result)
{
    $error_message = "Delete Product Failed - Contact Administrator";
    include('../database/database_error.php');
    exit();
}


header('Location: ../view/admin_edit_product.php');


?>
