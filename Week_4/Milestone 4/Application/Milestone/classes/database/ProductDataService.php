<?php
namespace classes\database;
use classes;
include_once $_SERVER['DOCUMENT_ROOT'] . '/Milestone/AutoLoader.php';

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-10-22
 * Class     : CST-236 Database Application Programming II
 * Professor : Nathan Braun
 * Assignment: Milestone
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Milestone
 * 2. Database Layer
 * 3. Product information
 * ---------------------------------------------------------------
 */

class ProductDataService
{

    public function __construct()
    {}
    
    public function findByProductName($searchPattern)
    {
        // Get Database Connection
        $database = new Database();
        $connection = $database->getConnect();
        
        // Define SQL prepare statement and bind values
        $sql = " SELECT * " .
               "   FROM products " .
               "  WHERE NAME LIKE :searchPattern " .
               "    AND deleted_flag = 0 " .
               "  ORDER BY NAME ASC";
        $statement = $connection->prepare($sql);

        $searchPattern = '%' . $searchPattern . '%';
        $statement->bindValue(':searchPattern', $searchPattern);
        
        $statement->bindValue(':searchPattern', $searchPattern);
        
        // Execute select query
        $statement->execute();
        
        // return records as associative array - could use fetchAll
        $statement->setFetchMode(\PDO::FETCH_ASSOC);
        
        // Read all users into a 2 dimensional Array where each Row in the Array
        // is a User ID at [0], First Name at [1], and Last Name at [2]
        $products = array();
        $index = 0;
        while ($row = $statement->fetch())
        {
            $products[$index] = array($row["ID"],
                                      $row["SCANCODE"],
                                      $row["NAME"],
                                      $row["DESCRIPTION"],
                                      $row["PRICE"],
                                      $row["DELETED_FLAG"]
            );
            ++$index;
        }
        
        // Close statement and connection
        $statement->closeCursor();
        $statement = null;
        $connection = null;
        $database = null;
        
        // Return array - even is blank
        return $products;
    }

    function getAllProducts()
    {
        // Get Database Connection
        $database = new Database();
        $connection = $database->getConnect();
        
        // Define SQL prepare statement and bind values
        $sql = "SELECT * " .
               "  FROM products " .
               " WHERE DELETED_FLAG = 0 " .
               " ORDER BY NAME ASC";
        $statement = $connection->prepare($sql);
        
        // Execute select query
        $statement->execute();
        
        // return records as associative array - could use fetchAll
        $statement->setFetchMode(\PDO::FETCH_ASSOC);
        
        // add records to products array
        $products = array();
        $index = 0;
        while ($row = $statement->fetch())
        {
            $products[$index] = array($row["ID"],
                                      $row["SCANCODE"],
                                      $row["NAME"],
                                      $row["DESCRIPTION"],
                                      $row["PRICE"],
                                      $row["DELETED_FLAG"]
            );
            ++$index;
        }
        
        // Close statement and connection
        $statement->closeCursor();
        $statement = null;
        $connection = null;
        $database = null;
        
        // Return array - even is blank
        return $products;
    }
    
    /**
     * @param id - integer
     * @return classes\model\Product Model
     */
    public function getProductById($id)
    {
        // Get Database Connection
        $database = new Database();
        $connection = $database->getConnect();
        
        // Define SQL prepare statement and bind values
        $sql = "SELECT * " .
               "  FROM products " .
               " WHERE ID = :id";
        $statement = $connection->prepare($sql);
        
        $statement->bindValue(':id', $id);
        
        // Execute select query
        $statement->execute();
        
        // return records as associative array - could use fetchAll
        $statement->setFetchMode(\PDO::FETCH_ASSOC);
        
        $row = $statement->fetch();
        
        $product = new classes\model\Product($row["ID"],
                                             $row["SCANCODE"],
                                             $row["NAME"],
                                             $row["DESCRIPTION"],
                                             $row["PRICE"],
                                             $row["DELETED_FLAG"]);
        
        // Close statement and connection
        $statement->closeCursor();
        $statement = null;
        $connection = null;
        $database = null;
        
        return $product;
        
    }
    
    public function delete($id)
    {
        try
        {
            // Get Database Connection
            $database = new Database();
            $connection = $database->getConnect();
            
            // Define SQL prepare statement and bind values
            // Set deleted flag = 1 (Yes)
            $sql = " UPDATE products " .
                   "    SET DELETED_FLAG = 1 " .
                   "  WHERE ID = :id";
            
            $statement = $connection->prepare($sql);
            $statement->bindValue(':id',  $id);
            
            // Execute delete stateent
            $statement->execute();
            
        } catch (\PDOException $e)
        {
            $error_message = $e->getMessage();
            include('../database/database_error.php');
            return false;
        }
        
        // Close statement and connection
        $statement->closeCursor();
        $statement = null;
        $connection = null;
        $database = null;
        
        return true;
    }
    
    /**
     * @param product Product - Model containing product information
     * @return boolean (true=success, false=error
     */
    public function update($product)
    {
        
        
        
        try
        {
            // Get Database Connection
            $database = new Database();
            $connection = $database->getConnect();
            
            // Define SQL prepare statement and bind values
            $sql = " UPDATE products " .
                   "    SET SCANCODE     = :scancode, " .
                   "        NAME         = :name, " .
                   "        DESCRIPTION  = :description, " .
                   "        PRICE        = :price, " .
                   "        DELETED_FLAG = :deleted_flag " .
                   "  WHERE ID = :id";
            $statement = $connection->prepare($sql);
            
            $statement->bindValue(':scancode',     $product->getScanCode());
            $statement->bindValue(':name',         $product->getName());
            $statement->bindValue(':description',  $product->getDescription());
            $statement->bindValue(':price',        $product->getPrice());
            $statement->bindValue(':deleted_flag', $product->getDeleted_flag());
            $statement->bindValue(':id',           $product->getId());
            
            // Execute insert query
            $statement->execute();
            
        } catch(\PDOException $e)
        {
            $error_message = $e->getMessage();
            include('../database/database_error.php');
            return false;
        }
        return true;
    }

    /**
     * @param product Product - Model containing product information
     * @return boolean (true=success, false=error
     */
    public function create($product)
    {
        try
        {
            // Get Database Connection
            $database = new Database();
            $connection = $database->getConnect();
            
            // Define SQL prepare statement and bind values
            $sql = "INSERT INTO products (SCANCODE, NAME, DESCRIPTION, PRICE, DELETED_FLAG) " .
                   "VALUES (:scancode, :name, :description, :price, :deleted_flag)";
            $statement = $connection->prepare($sql);
            
            $statement->bindValue(':scancode',     $product->getScanCode());
            $statement->bindValue(':name',         $product->getName());
            $statement->bindValue(':description',  $product->getDescription());
            $statement->bindValue(':price',        $product->getPrice());
            $statement->bindValue(':deleted_flag', $product->getDeleted_flag());
            
            // Execute insert query
            $statement->execute();
            
        } catch(\PDOException $e)
        {
            $error_message = $e->getMessage();
            include('database_error.php');
            return false;
        }
        return true;
    }
}

