<?php
namespace classes\business;
use classes;

include_once $_SERVER['DOCUMENT_ROOT'] . '/Milestone/AutoLoader.php';

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-11-07
 * Class     : CST-236 Database Application Programming II
 * Professor : Nathan Braun
 * Assignment: Milestone
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Milestone
 * 2. Business Layer
 * 3. Shopping Cart Information
 * ---------------------------------------------------------------
 */


class ShoppingCartBusinessService
{

    public function __construct()
    {}
    
    // Return an array of ShoppingCart objects
    public function getCart($user_id)
    {
        $service = new classes\database\ShoppingCartDataService();
        return $service->getCart($user_id);
    }

    // Add product and qty to cart - if already exists, update qty
    public function addToCart($user_id, $product_id, $qty)
    {        
        $service = new classes\database\ShoppingCartDataService();
        return $service->create($user_id, $product_id, $qty);
    }

    public function updateProductQty($user_id, $product_id, $new_qty)
    {
        $service = new classes\database\ShoppingCartDataService();
        
        if ($new_qty <= 0)
        {
            return $service->deleteProduct($user_id, $product_id);
        }
        else
        {
            return $service->update($user_id, $product_id, $new_qty);
        }
    }

    public function clearCart($user_id)
    {
        $service = new classes\database\ShoppingCartDataService();
        return $service->deleteCart($user_id);
    }
    
}

