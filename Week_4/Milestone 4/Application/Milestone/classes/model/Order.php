<?php
namespace classes\model;
include_once $_SERVER['DOCUMENT_ROOT'] . '/Milestone/AutoLoader.php';

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-11-07
 * Class     : CST-236 Database Application Programming II
 * Professor : Nathan Braun
 * Assignment: Milestone
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Milestone
 * 2. Business Layer
 * 3. Order model
 * ---------------------------------------------------------------
 */

class Order
{
    private $id;
    private $date;
    private $user_id;
    private $billing_address_id;
    private $shipping_address_id;
    
    public function __construct($id, $date, $user_id, $billing_address_id, $shipping_address_id)
    {
        $this->id = $id;
        $this->date = $date;
        $this->user_id = $user_id;
        $this->billing_address_id = $billing_address_id;
        $this->shipping_address_id = $shipping_address_id;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @return mixed
     */
    public function getUser_id()
    {
        return $this->user_id;
    }

    /**
     * @return mixed
     */
    public function getBilling_address_id()
    {
        return $this->billing_address_id;
    }

    /**
     * @return mixed
     */
    public function getShipping_address_id()
    {
        return $this->shipping_address_id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @param mixed $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * @param mixed $user_id
     */
    public function setUser_id($user_id)
    {
        $this->user_id = $user_id;
    }

    /**
     * @param mixed $billing_address_id
     */
    public function setBilling_address_id($billing_address_id)
    {
        $this->billing_address_id = $billing_address_id;
    }

    /**
     * @param mixed $shipping_address_id
     */
    public function setShipping_address_id($shipping_address_id)
    {
        $this->shipping_address_id = $shipping_address_id;
    }    
}

