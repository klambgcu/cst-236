<?php
namespace classes\view;
use classes;

include_once $_SERVER['DOCUMENT_ROOT'] . '/Milestone/AutoLoader.php';

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-11-07
 * Class     : CST-236 Database Application Programming II
 * Professor : Nathan Braun
 * Assignment: Milestone
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Milestone - Handle Shopping Cart
 * 2. Obtain form data
 * 3. Handle Data / Display
 * ---------------------------------------------------------------
 */

include_once '../../header.php';
include_once '../../securePage.php';
require_once '../../util_funcs.php';
?>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<link rel=stylesheet href="../../css/main_nav.css" />
<link rel=stylesheet href="../../css/post_entries.css" />
<title>STORE NAME HERE</title>
</head>
<body>

<?php require_once '../../_main_menu.php';?>

	<div align="center">
    	<hr><br />
    	<h1>Welcome - STORE NAME HERE!</h1>
    	<hr><br />
    	<h1>Order Check Out</h1><br />
	</div>

<br />
<hr>
Step 1. Shipping Address
<br><br><br><br>
<hr>
Step 2. Billing Address
<br><br><br><br>
<hr>
Step 3. Credit Card Information
<br><br><br><br>
<hr>
Step 4. Review Order
<br><br><br><br>
<hr>
Step 5. Submit Order
<br><br><br><br>
<button type="submit">Submit Order</button>
<hr>

</body>
</html>