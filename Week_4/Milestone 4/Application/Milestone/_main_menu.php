<?php
include_once $_SERVER['DOCUMENT_ROOT'] . '/Milestone/AutoLoader.php';
require_once 'util_funcs.php'
?>

<!--
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-10-17
 * Class     : CST-236 Database Application Programming II
 * Professor : Nathan Braun
 * Assignment: Milestone
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Main Menu (_main_menu.php)
 * 2.
 * 3.
 * ---------------------------------------------------------------
 -->


<nav id="main_nav">
<?php
$user_info = getUserInfo();

if (isset($user_info))
{
    $menu_name = "(" . $user_info[0]["FIRST_NAME"] . " " . $user_info[0]["LAST_NAME"] . ")";

    $role_id = $user_info[0]["ROLE_ID"];
    
    //
    // User is logged into the application
    //
    echo "  <ul>\r\n";
    echo "      <li><a href=\"/Milestone/index.php\">Home</a></li>\r\n";
    echo "      <li><a href=\"/Milestone/classes/view/productCatalog.php\">Products</a></li>\r\n";
    echo "      <li>\r\n";
    echo "          <a href=\"\">Account $menu_name &#9660;</a>\r\n";
    echo "          <ul>\r\n";
    echo "              <li><a href=\"/Milestone/classes/view/register.php\">Registration...</a></li>\r\n";
    echo "              <li><a href=\"/Milestone/classes/controller/logoutHandler.php\">Log Out</a></li>\r\n";
    echo "          </ul>\r\n";
    echo "      </li>\r\n";

    // ADMINISTRATOR MENU Role ID = (Basic = 1,  User Maint = 2, Product Maint = 3, Admin (Both) = 4)
    if ($role_id > 1)
    {
        echo "      <li>\r\n";
        echo "          <a href=\"\">Administrator &#9660;</a>\r\n";
        echo "          <ul>\r\n";

        if ($role_id == 2 || $role_id == 4)
        {
            echo "              <li><a href=\"/Milestone/classes/view/admin_edit_user.php\">User Admin...</a></li>\r\n";
        }
        
        if ($role_id == 3 || $role_id == 4)
        {
            echo "              <li><a href=\"/Milestone/classes/view/admin_edit_product.php\">Product Admin...</a></li>\r\n";
        }

        echo "          </ul>\r\n";
        echo "      </li>\r\n";
    }
    echo "      <li><a href=\"/Milestone/classes/view/shopping_cart.php\">Cart</a></li>\r\n";
    
    
    
    echo "  </ul>\r\n";
}
else
{
    //
    // User is NOT logged into the application
    //
    echo "  <ul>\r\n";
    echo "      <li><a href=\"/Milestone/index.php\">Home</a></li>\r\n";
    echo "      <li>\r\n";
    echo "          <a href=\"\">Account &#9660;</a>\r\n";
    echo "          <ul>\r\n";
    echo "              <li><a href=\"/Milestone/classes/view/register.php\">Registration...</a></li>\r\n";
    echo "              <li><a href=\"/Milestone/classes/view/login.php\">Login...</a></li>\r\n";
    echo "          </ul>\r\n";
    echo "      </li>\r\n";
    echo "  </ul>\r\n";
}
?>

</nav>

