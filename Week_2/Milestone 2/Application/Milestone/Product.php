<?php

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-10-24
 * Class     : CST-236 Database Application Programming II
 * Professor : Nathan Braun
 * Assignment: Milestone
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Milestone
 * 2. Business Layer
 * 3. Product mode
 * ---------------------------------------------------------------
 */

class Product
{
    private $id;
    private $scancode;
    private $name;
    private $description;
    private $price;
    
    public function __construct($id, $scancode, $name, $description, $price)
    {
        $this->id = $id;
        $this->$scancode = $scancode;
        $this->name = $name;
        $this->description = $description;
        $this->price = $price;
    }
    
    /**
     * @return int - product id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string - product scancode
     */
    public function getScanCode()
    {
        return $this->scancode;
    }

    /**
     * @return string - product name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string - product description
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return float - product price
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @param string $sancode
     */
    public function setScanCode($scancode)
    {
        $this->scancode = $scancode;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @param float $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }
}

