<?php

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-10-22
 * Class     : CST-236 Database Application Programming II
 * Professor : Nathan Braun
 * Assignment: Milestone
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Milestone
 * 2. Configuration
 * 3. Automatically load dependencies
 * ---------------------------------------------------------------
 */

spl_autoload_register(function($className) {
    $file = __DIR__ . '\\' . $className . '.php';
    $file = str_replace('\\', DIRECTORY_SEPARATOR, $file);
    if (file_exists($file)) {
        include $file;
    }
});
    
    
/*******





spl_autoload_register(function($class)
{
    $file = __DIR__ . '\\' . $class . '.php';
    $file = str_replace('\\', DIRECTORY_SEPARATOR, $file);
    
    // get the difference in folders between the location of autoloadere and the file that called autoloader.
    $lastdirectories = substr(getcwd(), strlen(__DIR__));
    
    echo "getcwd = : " . getcwd() . "<br />";
    echo "__DIR__ = : " . __DIR__ . "<br />";
    echo "last directories = : " . $lastdirectories . "<br />";
    
    // count the number of slashes (folder depth)
    $numberoflastdirectories = substr_count($lastdirectories, '\\');

    echo "number of directories different = : " . $numberoflastdirectories . "<br /> <br />";
    
    
    // this is the list of possible locations that classes are found in this app.
    $directories = ['businessService', 'businessService/model', 'database', 'presentation', 'presentation/handlers', 'presentation/views', 'presentation/views/login', 'utility'];
    
    // look inside each directory for the desired class.
    foreach($directories as $d) 
    {
        $currentdirectory = $d;
        for ($x = 0; $x < $numberoflastdirectories; $x++)
        {
            $currentdirectory = "../" . $currentdirectory;
        }
        $classfile = $currentdirectory . '/' . $class . '.php';
        
        if (is_readable($classfile))
        {
            if (require $d . '/' . $class . ".php")
            {
                break;
            }
        }
    }

});

********/
?>
