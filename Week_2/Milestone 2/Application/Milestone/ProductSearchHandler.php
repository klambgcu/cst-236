<?php

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-10-24
 * Class     : CST-236 Database Application Programming II
 * Professor : Nathan Braun
 * Assignment: Milestone
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Milestone - Handle Product Search
 * 2. Obtain form data
 * 3. Handle Data / Display
 * ---------------------------------------------------------------
 */

include_once 'header.php';
include_once 'securePage.php';
include_once 'Autoloader.php';
require_once 'util_funcs.php';

// Get criteria from product catalog search
$pattern = filter_input(INPUT_POST, "SearchPattern");

// Create a new busienss service object
$service = new ProductBusinessService();

// Load data from database for display
$products = $service->searchByProductName($pattern);

saveSearchInfo($products);


// print_r($products);

// Redirect to display data
header('Location: productCatalog.php');

?>