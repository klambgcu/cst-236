<?php

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-10-22
 * Class     : CST-236 Database Application Programming II
 * Professor : Nathan Braun
 * Assignment: Activity 2-2
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Security Service
 * 2. Validate User Login
 * 3. Handle Login Validation
 * ---------------------------------------------------------------
 */

class SecurityService
{
    // Variables for state
    private $Username;
    private $Password;

    public function __construct($n, $p)
    {
        // Initial state
        $this->Username = $n;
        $this->Password = $p;
    }
    
    public function authenticate($n, $p)
    {
        // Compare login versus state
        if ($this->Username == $n && $this->Password == $p)
            return true;
        else 
            return false;
    }
}

