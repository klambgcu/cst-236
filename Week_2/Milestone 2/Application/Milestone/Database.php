<?php

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-10-22
 * Class     : CST-236 Database Application Programming II
 * Professor : Nathan Braun
 * Assignment: Activity 2.1
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Activity 2
 * 2. Persistence Layer
 * 3. Connection manager
 * ---------------------------------------------------------------
 */

include_once 'Autoloader.php';

class Database
{

    public function __construct()
    {}
    
    //
    // Create a database PDO connection
    // Returns the connection
    // Throw exception to database_error display form
    //
    public function getConnect()
    {
        // Define local / development database connection parameters
        $connect_string = 'mysql:host=localhost:3306;dbname=cst-236';
        $db_username = "root";
        $db_password = "root";
        
        // Define azure / publish database connection parameters
        // $connect_string = 'mysql:host=127.0.0.1:49921;dbname=cst-126';
        // $db_username = "azure";
        // $db_password = "6#vWHD_$";
        
        try
        {
            // Create a PDO object
            $db_connection = new PDO($connect_string, $db_username, $db_password);
            $db_connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $e)
        {
            echo "Database Connection Error: " . $e->getMessage() . "<br />";
            exit();
        }
        
        return $db_connection;
    }
    
}

