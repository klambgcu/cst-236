<!--
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-10-24
 * Class     : CST-236 Database Application Programming II
 * Professor : Nathan Braun
 * Assignment: Milestone
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Milestone (_displayProduct.php) display table of products
 * 2. Reusable functions
 * ---------------------------------------------------------------
 -->

<table id="post_entries" class="display">
  <thead>
    <tr>
        <th>ID</th>
        <th>Scan Code</th>
        <th>Name</th>
        <th>Description</th>
        <th>Price</th>
    </tr>
  </thead>
  <tbody>
<?php

require_once 'util_funcs.php';

$products = getSearchInfo();

if (isset($products))
{
    foreach($products as $p) 
    {
        echo "  <tr>\n";
        echo "      <td>" . $p[0] . "</td>\n";
        echo "      <td>" . $p[1] . "</td>\n";
        echo "      <td>" . $p[2] . "</td>\n";
        echo "      <td>" . $p[3] . "</td>\n";
        echo "      <td>" . $p[4] . "</td>\n";
        echo "  </tr>\n";
	}
}
	
	
 ?>

  </tbody>
</table>
