<!--
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-10-17
 * Class     : CST-236 Database Application Programming II
 * Professor : Nathan Braun
 * Assignment: Milestone
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Milestone (_displayUsers.php) display table of users for edit
 * 2. Reusable functions
 * ---------------------------------------------------------------
 -->

<table id="post_entries">
    <tr>
        <th>ID</th>
        <th>First Name</th>
        <th>Last Name</th>
        <th>Email</th>
        <th>Mobile</th>
        <th>Password</th>
        <th>Birthdate</th>
        <th>Gender</th>
    </tr>

<?php
    $gender = array();
    $gender[0] = "Male";
    $gender[1] = "Female";

    foreach ($users as $u)
    {
        echo "  <tr>\n";
        echo "      <td>" . $u[0] . "</td>\n";
        echo "      <td>" . $u[1] . "</td>\n";
        echo "      <td>" . $u[2] . "</td>\n";
        echo "      <td>" . $u[3] . "</td>\n";
        echo "      <td>" . $u[4] . "</td>\n";
        echo "      <td>" . $u[5] . "</td>\n";
        echo "      <td>" . $u[6] . "</td>\n";
        echo "      <td>" . $gender[$u[7]] . "</td>\n";
        echo "  </tr>\n";
	}
 ?>

</table>
