<?php

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-10-22
 * Class     : CST-236 Database Application Programming II
 * Professor : Nathan Braun
 * Assignment: Activity 2.1
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Activity 2
 * 2. Business Layer
 * 3. User information
 * ---------------------------------------------------------------
 */

include 'Autoloader.php';

/**
 * @author Kelly Lamb
 *
 */
class UserDataService
{

    public function __construct()
    {}
    
    public function findByFirstName($searchPattern)
    {
        // Get Database Connection
        $database = new Database();
        $connection = $database->getConnect();
        
        // Define SQL prepare statement and bind values
        $sql = "SELECT * " .
               "  FROM users " .
               " WHERE FIRST_NAME LIKE :searchPattern ORDER BY LAST_NAME ASC, FIRST_NAME ASC";
        $statement = $connection->prepare($sql);
        
        $searchPattern = '%' . $searchPattern . '%';
        $statement->bindValue(':searchPattern', $searchPattern);
        
        // Execute select query
        $statement->execute();
        
        // return records as associative array - could use fetchAll
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        
        // Read all users into a 2 dimensional Array where each Row in the Array
        // is a User ID at [0], First Name at [1], and Last Name at [2]
        $users = array();
        $index = 0;
        while ($row = $statement->fetch())
        {
            $users[$index] = array($row["ID"],
                                   $row["FIRST_NAME"],
                                   $row["LAST_NAME"],
                                   $row["EMAIL"],
                                   $row["MOBILE"],
                                   $row["PASSWORD"],
                                   $row["BIRTHDATE"],
                                   $row["GENDER"],
                                   $row["ROLE_ID"]);
            ++$index;
        }
        
        // Close statement and connection
        $statement->closeCursor();
        $statement = null;
        $connection = null;
        
        // Return null if zero records found
        if (count($users) == 0)
        {
            return null;

        }
        else 
        {
            return $users;
        }
    }

    public function getAllUsers()
    {
        // Get Database Connection
        $database = new Database();
        $connection = $database->getConnect();
        
        // Define SQL prepare statement and bind values
        $sql = "SELECT u.* " .
               "  FROM users u " .
               " ORDER BY u.LAST_NAME, u.FIRST_NAME";
        $statement = $connection->prepare($sql);
        
        // Execute select query
        $statement->execute();
        
        // return records as associative array - could use fetchAll
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        
        // add records to users array
        $users = array();
        $index = 0;
        while ($row = $statement->fetch())
        {
            $users[$index] = array($row["ID"], 
                                   $row["FIRST_NAME"],
                                   $row["LAST_NAME"],
                                   $row["EMAIL"],
                                   $row["MOBILE"],
                                   $row["PASSWORD"],
                                   $row["BIRTHDATE"],
                                   $row["GENDER"],
                                   $row["ROLE_ID"]);
            ++$index;
        }
        
        // Close statement and connection
        $statement->closeCursor();
        $statement = null;
        $connection = null;
        $database = null;
        
        if (count($users) == 0)
        {
            return null;
            
        }
        else
        {
            return $users;
        }
    }
    
    
    /**
     * @param user User - Model containing user information
     * @return boolean (true=success, false=error
     */
    public function create($user)
    {
        try
        {
            // Get Database Connection
            $database = new Database();
            $connection = $database->getConnect();
            
            // Define SQL prepare statement and bind values
            $sql = "INSERT INTO users (FIRST_NAME, LAST_NAME, EMAIL, MOBILE, PASSWORD, BIRTHDATE, GENDER, ROLE_ID) " .
                   "VALUES (:firstname, :lastname, :email, :mobile, :password, :bdate_str, :gender, :role_id)";
            $statement = $connection->prepare($sql);
            
            $statement->bindValue(':firstname', $user->getFirst_name());
            $statement->bindValue(':lastname',  $user->getLast_name());
            $statement->bindValue(':email',     $user->getEmail());
            $statement->bindValue(':mobile',    $user->getMobile());
            $statement->bindValue(':password',  $user->getPassword());
            $statement->bindValue(':bdate_str', $user->getBirthdate());
            $statement->bindValue(':gender',    $user->getGender());
            $statement->bindValue(':role_id',   $user->getRole_id());
            
            // Execute insert query
            $statement->execute();

        } catch(PDOException $e)
        {
            $error_message = $e->getMessage();
            include('database_error.php');
            return false;
        }
        return true;
    }
    
}

