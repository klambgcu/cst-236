<?php

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-10-22
 * Class     : CST-236 Database Application Programming II
 * Professor : Nathan Braun
 * Assignment: Milestone
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Milestone
 * 2. Business Layer
 * 3. User information
 * ---------------------------------------------------------------
 */

include_once 'Autoloader.php';

class UserBusinessService
{

    public function __construct()
    {}
    
    public function searchByFirstName($pattern)
    {
        $service = new UserDataService();
        return $service->findByFirstName($pattern);
    }
    
    public function getAllUsers()
    {
        $service = new UserDataService();
        return $service->getAllUsers();
    }
    
    public function createUser($user)
    {
        $service = new UserDataService();
        return $service->create($user);
    }
    
}

