<?php

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-10-22
 * Class     : CST-236 Database Application Programming II
 * Professor : Nathan Braun
 * Assignment: Milestone
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Milestone
 * 2. Business Layer
 * 3. Product information
 * ---------------------------------------------------------------
 */

include_once 'Autoloader.php';

class ProductDataService
{

    public function __construct()
    {}
    
    public function findByProductName($searchPattern)
    {
        // Get Database Connection
        $database = new Database();
        $connection = $database->getConnect();
        
        // Define SQL prepare statement and bind values
        $sql = "SELECT * FROM products WHERE NAME LIKE :searchPattern ORDER BY NAME ASC";
        $statement = $connection->prepare($sql);

        $searchPattern = '%' . $searchPattern . '%';
        $statement->bindValue(':searchPattern', $searchPattern);
        
        $statement->bindValue(':searchPattern', $searchPattern);
        
        // Execute select query
        $statement->execute();
        
        // return records as associative array - could use fetchAll
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        
        // Read all users into a 2 dimensional Array where each Row in the Array
        // is a User ID at [0], First Name at [1], and Last Name at [2]
        $products = array();
        $index = 0;
        while ($row = $statement->fetch())
        {
            $products[$index] = array($row["ID"],
                                      $row["SCANCODE"],
                                      $row["NAME"],
                                      $row["DESCRIPTION"],
                                      $row["PRICE"]);
            ++$index;
        }
        
        // Close statement and connection
        $statement->closeCursor();
        $statement = null;
        $connection = null;
        $database = null;
        
        // Return array - even is blank
        return $products;
    }

    function getAllProduct()
    {
        // Get Database Connection
        $database = new Database();
        $connection = $database->getConnect();
        
        // Define SQL prepare statement and bind values
        $sql = "SELECT p.* " .
               "  FROM products p " .
               " ORDER BY p.NAME ASC, u.FIRST_NAME";
        $statement = $connection->prepare($sql);
        
        // Execute select query
        $statement->execute();
        
        // return records as associative array - could use fetchAll
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        
        // add records to products array
        $products = array();
        $index = 0;
        while ($row = $statement->fetch())
        {
            $products[$index] = array($row["ID"],
                                      $row["SCANCODE"],
                                      $row["NAME"],
                                      $row["DESCRIPTION"],
                                      $row["PRICE"]);
            ++$index;
        }
        
        // Close statement and connection
        $statement->closeCursor();
        $statement = null;
        $connection = null;
        $database = null;
        
        // Return array - even is blank
        return $products;
    }

    /**
     * @param product Product - Model containing product information
     * @return boolean (true=success, false=error
     */
    public function create($product)
    {
        try
        {
            // Get Database Connection
            $database = new Database();
            $connection = $database->getConnect();
            
            // Define SQL prepare statement and bind values
            $sql = "INSERT INTO products (SCANCODE, NAME, DESCRIPTION, PRICE) " .
                   "VALUES (:scancode, :name, :description, :price)";
            $statement = $connection->prepare($sql);
            
            $statement->bindValue(':scancode',    $product->getScanCode());
            $statement->bindValue(':name',        $product->getName());
            $statement->bindValue(':description', $product->getDescription());
            $statement->bindValue(':price',       $product->getPrice());
            
            // Execute insert query
            $statement->execute();
            
        } catch(PDOException $e)
        {
            $error_message = $e->getMessage();
            include('database_error.php');
            return false;
        }
        return true;
    }
}

