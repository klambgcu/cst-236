<?php

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-10-22
 * Class     : CST-236 Database Application Programming II
 * Professor : Nathan Braun
 * Assignment: Milestone
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Milestone
 * 2. Business Layer
 * 3. Product information
 * ---------------------------------------------------------------
 */

include_once 'Autoloader.php';

class ProductBusinessService
{

    public function __construct()
    {}
    
    public function searchByProductName($pattern)
    {
        $service = new ProductDataService();
        return $service->findByProductName($pattern);
    }

    public function getAllProducts()
    {
        $service = new ProductDataService();
        return $service->getAllProducts();
    }

    public function createProduct($product)
    {
        $service = new ProductDataService();
        return $service->create($product);
    }
    
}

