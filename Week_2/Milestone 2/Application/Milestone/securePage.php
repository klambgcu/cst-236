<?php

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-10-22
 * Class     : CST-236 Database Application Programming II
 * Professor : Nathan Braun
 * Assignment: Milestone
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Page Level Security
 * 2. Validate login and/or redirect to login form
 * 3.
 * ---------------------------------------------------------------
 */

include 'header.php';

$test = $_SESSION["principle"];

if (is_null($test) || empty($test) || $test == false)
{
    header("Location: login.php");
}

?>
