<?php

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-10-22
 * Class     : CST-236 Database Application Programming II
 * Professor : Nathan Braun
 * Assignment: Activity 2-2
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Handle Login Entry
 * 2. Obtain form data
 * 3. Handle Login Validation
 * ---------------------------------------------------------------
 */

include 'header.php';

require_once 'SecurityService.php';

// store registration parameters
$username = filter_input(INPUT_POST,'Username');
$password = filter_input(INPUT_POST,'Password');

// Validate user entry
$valid_input = true;

// Validate username
// Note: Applying required on the html field(s) makes this unnecessary
if (is_null($username) || empty($username)) {
    $valid_input = false;
    echo "The Username field is a required field and cannot be blank.<br />";
}

// Validate password
if (is_null($password) || empty($password)) {
    $valid_input = false;
    echo "The Password field is a required field and cannot be blank.<br />";
}

// Check and continue only if input fields are valid
if ($valid_input)
{
    // Configure security service and authenicate user
    $service = new SecurityService("kelly", "lamb1234");
    
    if ($service->authenticate($username, $password))
    {
        // Valid user
        $_SESSION["principle"] = true;
        include('loginPassed.php');
    }
    else 
    {
        // Invalid user
        $_SESSION["principle"] = false;
        include('loginFailed.php');
    }

}

?>
