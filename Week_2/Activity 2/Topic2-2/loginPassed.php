<?php

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-10-22
 * Class     : CST-236 Database Application Programming II
 * Professor : Nathan Braun
 * Assignment: Activity 2-2
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Page Level Security
 * 2. Displays login attempted success
 * 3. Secures this page and redirects to login.html
 * ---------------------------------------------------------------
 */

include 'header.php';
include 'securePage.php';

echo "<h1>Login attempt successful!</h1> <br />";

?>
