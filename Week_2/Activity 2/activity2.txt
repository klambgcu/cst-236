CREATE DATABASE ICA17;
USE ICA17;

DROP TABLE IF EXISTS `USERS`;
CREATE TABLE `USERS`
(
	`ID`           INT(11)      NOT NULL AUTO_INCREMENT,
	`FIRST_NAME`   VARCHAR(100) NOT NULL,
	`LAST_NAME`    VARCHAR(100) NOT NULL
	PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

LOAD DATA LOCAL INFILE /tmp/users.txt INTO TABLE users FIELDS TERMINATED BY '\t' (FIRST_NAME, LAST_NAME);

-- All last names containing a letter a
SELECT * FROM users WHERE LAST_NAME LIKE '%a%';

-- All first names containing a letter a
SELECT * FROM users WHERE FIRST_NAME LIKE '%a%';

-- All records where first or last name contain a letter a
SELECT * FROM users WHERE FIRST_NAME LIKE '%a%' OR LAST_NAME LIKE '%a%';