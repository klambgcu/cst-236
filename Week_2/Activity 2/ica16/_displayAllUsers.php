<!-- 
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-10-22
 * Class     : CST-236 Database Application Programming II
 * Professor : Nathan Braun
 * Assignment: Activity 2.1
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Activity 2
 * 2. Reusable function - display users table
 * ---------------------------------------------------------------
 -->

<table border=1>
    <tr>
        <th>ID</th>
        <th>First Name</th>
        <th>Last Name</th>
    </tr>

<?php
    for($x=0;$x < count($users); $x++)
    {
        echo "  <tr>\n";
        echo "      <td>" . $users[$x][0] . "</td>\n";
        echo "      <td>" . $users[$x][1] . "</td>\n";
        echo "      <td>" . $users[$x][2] . "</td>\n";
        echo "  </tr>\n";
	}
 ?>

</table>
