<?php

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-10-22
 * Class     : CST-236 Database Application Programming II
 * Professor : Nathan Braun
 * Assignment: Activity 2.1
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Activity 2
 * 2. Configuration
 * 3. Automatically load dependencies
 * ---------------------------------------------------------------
 */

spl_autoload_register(function($className) {
    $file = __DIR__ . '\\' . $className . '.php';
    $file = str_replace('\\', DIRECTORY_SEPARATOR, $file);
    if (file_exists($file)) {
        include $file;
    }
});


?>
