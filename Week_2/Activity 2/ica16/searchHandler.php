<?php

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-10-22
 * Class     : CST-236 Database Application Programming II
 * Professor : Nathan Braun
 * Assignment: Activity 2.1
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Activity 2
 * 2. Obtain form data (search Pattern)
 * 3. Searches the database
 * ---------------------------------------------------------------
 */

include 'Autoloader.php';

// store search pattern parameter
$searchPattern = filter_input(INPUT_POST,'SearchPattern');

// Validate user entry
$valid_input = true;

// Validate search pattern
// Note: Applying required on the html field(s) makes this unnecessary
if (is_null($searchPattern) || empty($searchPattern)) {
    $valid_input = false;
    echo "The Search Pattern field is a required field and cannot be blank.<br />";
}

// Check and continue only if input fields are valid 
if ($valid_input) {

    echo "<h1>Search Criteria: " . $searchPattern . "</h1><br /><br />";
    
    // Send in the search pattern
    $service = new UserBusinessService();
    $users = $service->searchByFirstName($searchPattern);
    if (is_null($users))
        echo "There are no users matching the search pattern criteria. Please try again.";
    else
        include('_displayAllUsers.php');
        
}

?>
