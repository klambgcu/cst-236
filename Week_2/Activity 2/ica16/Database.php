<?php

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-10-22
 * Class     : CST-236 Database Application Programming II
 * Professor : Nathan Braun
 * Assignment: Activity 2.1
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Activity 2
 * 2. Persistence Layer
 * 3. Connection manager
 * ---------------------------------------------------------------
 */

include 'Autoloader.php';

class Database
{

    public function __construct()
    {}
    
    //
    // Create a database PDO connection
    // Returns the connection
    // Throw exception to database_error display form
    //
    public function getConnect()
    {
        // Define database connection parameters
        $connect_string = 'mysql:host=localhost;dbname=ica17';
        $db_username = "root";
        $db_password = "root";
        
        try
        {
            // Create a PDO object
            $db_connection = new PDO($connect_string, $db_username, $db_password);
            $db_connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $e)
        {
            echo "Database Connection Error: " . $e->getMessage() . "<br />";
            exit();
        }
        
        return $db_connection;
    }
    
}

