<?php

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-10-22
 * Class     : CST-236 Database Application Programming II
 * Professor : Nathan Braun
 * Assignment: Activity 2.1
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Activity 2
 * 2. Business Layer
 * 3. User information
 * ---------------------------------------------------------------
 */

include 'Autoloader.php';

class UserBusinessService
{

    public function __construct()
    {}
    
    public function searchByFirstName($pattern)
    {
        $service = new UserDataService();
        return $service->findByFirstName($pattern);
    }
    
}

