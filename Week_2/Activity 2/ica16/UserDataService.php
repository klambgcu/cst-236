<?php

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-10-22
 * Class     : CST-236 Database Application Programming II
 * Professor : Nathan Braun
 * Assignment: Activity 2.1
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Activity 2
 * 2. Business Layer
 * 3. User information
 * ---------------------------------------------------------------
 */

include 'Autoloader.php';

class UserDataService
{

    public function __construct()
    {}
    
    public function findByFirstName($searchPattern)
    {
        // Get Database Connection
        $database = new Database();
        $db = $database->getConnect();
        
        // Define SQL prepare statement and bind values
        $sql = "SELECT ID, FIRST_NAME, LAST_NAME FROM users WHERE FIRST_NAME LIKE :searchPattern";
        $statement1 = $db->prepare($sql);
        $statement1->bindValue(':searchPattern', $searchPattern);
        
        // Execute select query
        $statement1->execute();
        
        // return records as associative array - could use fetchAll
        $statement1->setFetchMode(PDO::FETCH_ASSOC);
        
        // Read all users into a 2 dimensional Array where each Row in the Array
        // is a User ID at [0], First Name at [1], and Last Name at [2]
        $users = array();
        $index = 0;
        while ($row = $statement1->fetch())
        {
            $users[$index] = array($row["ID"], $row["FIRST_NAME"], $row["LAST_NAME"]);
            ++$index;
        }
        
        // Close statement and connection
        $statement1->closeCursor();
        $statement1 = null;
        $db = null;
        
        // Return null if zero records found
        if (count($users) == 0)
            return null;
            else
                return $users;
    }
    
}

